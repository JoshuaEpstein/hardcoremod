package joshuaepstein.hardcoremod.init;

import joshuaepstein.hardcoremod.Main;
import joshuaepstein.hardcoremod.block.BasicBlock;
import joshuaepstein.hardcoremod.block.HardcoreOreBlock;
import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.OreBlock;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.TallBlockItem;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;

public class ModBlocks {
    public static final BasicBlock ULTRA_DIAMOND_BLOCK = new BasicBlock(AbstractBlock.Properties.copy(Blocks.DIAMOND_BLOCK), 0);
    public static final Block RUBY_BLOCK = new Block(AbstractBlock.Properties.copy(Blocks.EMERALD_BLOCK));
    public static final Block CRYSTAL_BLOCK = new Block(AbstractBlock.Properties.copy(Blocks.EMERALD_BLOCK));
    public static final Block IOLITE_BLOCK = new Block(AbstractBlock.Properties.copy(Blocks.EMERALD_BLOCK));
    public static final OreBlock HARDCORE_DIAMOND_ORE = new HardcoreOreBlock();
    public static final Block NETHER_SCRAP_ORE = new HardcoreOreBlock();
    public static final Block RUBY_GEM_ORE = new HardcoreOreBlock();
    public static final Block CRYSTAL_GEM_ORE = new HardcoreOreBlock();
    public static final Block IOLITE_GEM_ORE = new HardcoreOreBlock();
    public static final Block SAPPHIRE_BLOCK = new Block(AbstractBlock.Properties.copy(Blocks.EMERALD_BLOCK));
    public static final Block SAPPHIRE_GEM_ORE = new HardcoreOreBlock();
    public static final Block TOPAZ_BLOCK = new Block(AbstractBlock.Properties.copy(Blocks.EMERALD_BLOCK));
    public static final Block TOPAZ_GEM_ORE = new HardcoreOreBlock();
    public static final BasicBlock VERTICAL_SPRUCE_SLAB = new BasicBlock(AbstractBlock.Properties.copy(Blocks.SPRUCE_PLANKS), 0);


    public static void registerBlocks(RegistryEvent.Register<Block> event) {
        registerBlock(event, ULTRA_DIAMOND_BLOCK, Main.id("ultra_diamond_block"));
        registerBlock(event, HARDCORE_DIAMOND_ORE, Main.id("ore_ultra_diamond"));
        registerBlock(event, NETHER_SCRAP_ORE, Main.id("ultra_ancient_debris"));
        registerBlock(event, RUBY_GEM_ORE, Main.id("ore_ruby"));
        registerBlock(event, RUBY_BLOCK, Main.id("ruby_block"));
        registerBlock(event, CRYSTAL_BLOCK, Main.id("crystal_block"));
        registerBlock(event, CRYSTAL_GEM_ORE, Main.id("ore_crystal"));
        registerBlock(event, IOLITE_BLOCK, Main.id("iolite_block"));
        registerBlock(event, IOLITE_GEM_ORE, Main.id("ore_iolite"));
        registerBlock(event, SAPPHIRE_BLOCK, Main.id("sapphire_block"));
        registerBlock(event, SAPPHIRE_GEM_ORE, Main.id("ore_sapphire"));
        registerBlock(event, TOPAZ_BLOCK, Main.id("topaz_block"));
        registerBlock(event, TOPAZ_GEM_ORE, Main.id("ore_topaz"));
        registerBlock(event, VERTICAL_SPRUCE_SLAB, Main.id("vertical_spruce_slab"));
    }


    public static void registerBlockItems(RegistryEvent.Register<Item> event) {
        registerBlockItem(event, ULTRA_DIAMOND_BLOCK);
        registerBlockItem(event, HARDCORE_DIAMOND_ORE);
        registerBlockItem(event, NETHER_SCRAP_ORE);
        registerBlockItem(event, RUBY_GEM_ORE);
        registerBlockItem(event, RUBY_BLOCK);
        registerBlockItem(event, CRYSTAL_BLOCK);
        registerBlockItem(event, CRYSTAL_GEM_ORE);
        registerBlockItem(event, IOLITE_BLOCK);
        registerBlockItem(event, IOLITE_GEM_ORE);
        registerBlockItem(event, SAPPHIRE_BLOCK);
        registerBlockItem(event, SAPPHIRE_GEM_ORE);
        registerBlockItem(event, TOPAZ_BLOCK);
        registerBlockItem(event, TOPAZ_GEM_ORE);
    }

    /* --------------------------------------------- */
    public static void registerTileEntities(RegistryEvent.Register<TileEntityType<?>> event) {
//        registerTileEntity(event, ULTRA_CHEST, Main.id("ultra_chest_tile_entity"));
    }
    public static void registerTileEntityRenderers() {

    }
    private static void registerBlock(RegistryEvent.Register<Block> event, Block block, ResourceLocation id) {
        block.setRegistryName(id);
        event.getRegistry().register(block);
    }

    private static <T extends TileEntity> void registerTileEntity(RegistryEvent.Register<TileEntityType<?>> event, TileEntityType<?> type, ResourceLocation id) {
        type.setRegistryName(id);
        event.getRegistry().register(type);
    }

    private static void registerBlockItem(RegistryEvent.Register<Item> event, Block block) {
        BlockItem blockItem = new BlockItem(block, new Item.Properties().tab(ModItems.HARDCORE_MOD_GROUP).stacksTo(64));
        blockItem.setRegistryName(block.getRegistryName());
        event.getRegistry().register(blockItem);
    }

    private static void registerBlockItem(RegistryEvent.Register<Item> event, Block block, int maxStackSize, boolean showGlint) {
        BlockItem blockItem = new BlockItem(block, new Item.Properties().tab(ModItems.HARDCORE_MOD_GROUP).stacksTo(maxStackSize)) {
            @Override
            public boolean isFoil(ItemStack stack) {
                return showGlint;
            }
        };

        blockItem.setRegistryName(block.getRegistryName());
        event.getRegistry().register(blockItem);
    }

    private static void registerBlockItem(RegistryEvent.Register<Item> event, Block block, int maxStackSize) {
        BlockItem blockItem = new BlockItem(block, new Item.Properties().tab(ModItems.HARDCORE_MOD_GROUP).stacksTo(maxStackSize));
        blockItem.setRegistryName(block.getRegistryName());
        event.getRegistry().register(blockItem);
    }

    private static void registerBlockItem(RegistryEvent.Register<Item> event, Block block, BlockItem blockItem) {
        blockItem.setRegistryName(block.getRegistryName());
        event.getRegistry().register(blockItem);
    }

    private static void registerTallBlockItem(RegistryEvent.Register<Item> event, Block block) {
        TallBlockItem tallBlockItem = new TallBlockItem(block, new Item.Properties()
                .tab(ModItems.HARDCORE_MOD_GROUP)
                .stacksTo(64));
        tallBlockItem.setRegistryName(block.getRegistryName());
        event.getRegistry().register(tallBlockItem);
    }
}
