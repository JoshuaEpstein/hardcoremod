package joshuaepstein.hardcoremod.init;

import joshuaepstein.hardcoremod.Main;
//import joshuaepstein.hardcoremod.recipe.MysteryStewRecipe;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.SpecialRecipeSerializer;
import net.minecraftforge.event.RegistryEvent;

public class ModRecipes {

	public static class Serializer {
//		public static SpecialRecipeSerializer<CrateRecipe> CRAFTING_CRATE_RECIPE;

		public static void register(RegistryEvent.Register<IRecipeSerializer<?>> event) {
//			CRAFTING_CRATE_RECIPE = register(event, "crafting_special_mystery_stew", new SpecialRecipeSerializer<>(CrateRecipe::new));
		}

		private static <T extends IRecipe<?>> SpecialRecipeSerializer<T> register(RegistryEvent.Register<IRecipeSerializer<?>> event, String name, SpecialRecipeSerializer<T> serializer) {
			serializer.setRegistryName(Main.id(name));
			event.getRegistry().register(serializer);
			return serializer;
		}
	}

}
