package joshuaepstein.hardcoremod.init;

import joshuaepstein.hardcoremod.Main;
//import joshuaepstein.hardcoremod.network.message.*;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkRegistry;
import net.minecraftforge.fml.network.simple.SimpleChannel;

public class ModNetwork {

    private static final String NETWORK_VERSION = "0.21.0";

    public static final SimpleChannel CHANNEL = NetworkRegistry.newSimpleChannel(
            new ResourceLocation(Main.MOD_ID, "network"),
            () -> NETWORK_VERSION,
            version -> version.equals(NETWORK_VERSION), // Client acceptance predicate
            version -> version.equals(NETWORK_VERSION) // Server acceptance predicate
    );

    private static int ID = 0;

    public static void initialize() {
//        CHANNEL.registerMessage(nextId(), OpenSkillTreeMessage.class,
//                OpenSkillTreeMessage::encode,
//                OpenSkillTreeMessage::decode,
//                OpenSkillTreeMessage::handle);
    }

    public static int nextId() {
        return ID++;
    }

}
