package joshuaepstein.hardcoremod.init;

import joshuaepstein.hardcoremod.Main;
import joshuaepstein.hardcoremod.util.LazySoundType;
import net.minecraft.item.Items;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.event.RegistryEvent;

public class ModSounds {

    public static SoundEvent RAFFLE_SFX;
    public static SoundEvent CONFETTI_SFX;
    public static SoundEvent SUCCESS_SFX;
//    public static LazySoundType GEM = new LazySoundType();

    public static void registerSounds(RegistryEvent.Register<SoundEvent> event) {
        RAFFLE_SFX = registerSound(event, "raffle");
        CONFETTI_SFX = registerSound(event, "confetti");
        SUCCESS_SFX = registerSound(event, "success");
    }

    public static void registerSoundTypes() {
//        GEM.initialize(0.75f, 1f, GEM_BREAK, null, null, GEM_HIT, null);
    }

    /* ---------------------------- */

    private static SoundEvent registerSound(RegistryEvent.Register<SoundEvent> event, String soundName) {
        ResourceLocation location = Main.id(soundName);
        SoundEvent soundEvent = new SoundEvent(location);
        soundEvent.setRegistryName(location);
        event.getRegistry().register(soundEvent);
        return soundEvent;
    }

}
