package joshuaepstein.hardcoremod.init;

import net.minecraft.item.Food;
import net.minecraft.potion.Effects;

public class Foods {

    public static final Food PIZZA = (new Food.Builder()).nutrition(6).saturationMod(0.8F).build();
    public static final Food BURGER = (new Food.Builder()).nutrition(7).saturationMod(1.1F).build();


}
