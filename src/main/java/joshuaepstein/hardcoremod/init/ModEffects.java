package joshuaepstein.hardcoremod.init;

import joshuaepstein.hardcoremod.Main;

import net.minecraft.potion.Effect;
import net.minecraft.potion.EffectType;
import net.minecraftforge.event.RegistryEvent;

import java.awt.*;

public class ModEffects {

//    public static final Effect BLANK_EFFECT = new BlankEffectEffect(EffectType.BENEFICIAL, Color.GRAY.getRGB(), Main.id("blank_effect"));

    public static void register(RegistryEvent.Register<Effect> event) {
//        register(BLANK_EFFECT, event);
    }

    /* --------------------------------------------- */

    private static <T extends Effect> void register(T effect, RegistryEvent.Register<Effect> event) {
        event.getRegistry().register(effect);
    }

}
