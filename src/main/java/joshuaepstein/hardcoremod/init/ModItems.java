package joshuaepstein.hardcoremod.init;

import joshuaepstein.hardcoremod.Main;
import joshuaepstein.hardcoremod.helper.PaxelUltraTier;
import joshuaepstein.hardcoremod.helper.UltraTier;
import joshuaepstein.hardcoremod.item.SwordItem;
import joshuaepstein.hardcoremod.item.*;
import joshuaepstein.hardcoremod.item.paxels.FortunatePaxelItem;
import joshuaepstein.hardcoremod.item.paxels.ReachPaxelItem;
import joshuaepstein.hardcoremod.item.paxels.RushPaxelItem;
import net.minecraft.block.Blocks;
import net.minecraft.fluid.Fluids;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.*;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.registries.IForgeRegistry;
import java.util.Random;

public class ModItems {

    public static ItemGroup HARDCORE_MOD_GROUP = new ItemGroup(Main.MOD_ID) {
        @Override
        public ItemStack makeIcon() {
            return new ItemStack(ULTRA_INGOT);
        }

        @Override
        public boolean hasSearchBar() {
            return false;
        }
    };
    public static LootableItem ULTRA_BOX = new LootableItem(Main.id("ultra_box"), new Item.Properties().tab(HARDCORE_MOD_GROUP), () -> ModConfigs.ULTRA_BOX.POOL.getRandom(new Random()).toStack(), true, "Right click to get a random item", TextFormatting.BLUE);
    public static BasicItem HARDCORE_DIAMOND = new BasicItem(Main.id("ultra_diamond"), new Item.Properties().tab(HARDCORE_MOD_GROUP).stacksTo(64), false, "", TextFormatting.WHITE);
    public static MagnetItem WEAK_MAGNET = new MagnetItem(Main.id("magnet_weak"), new Item.Properties().tab(HARDCORE_MOD_GROUP).setNoRepair().stacksTo(1), 5, 2500);
    public static MagnetItem STRONG_MAGNET = new MagnetItem(Main.id("magnet_strong"), new Item.Properties().tab(HARDCORE_MOD_GROUP).stacksTo(1).setNoRepair(), 9, 5000);
    public static MagnetItem JUST_ANOTHER_MAGNET = new MagnetItem(Main.id("magnet_just_another"), new Item.Properties().tab(HARDCORE_MOD_GROUP).stacksTo(1).setNoRepair(), 14, 6000);
    public static MagnetRepairCoreItem WEAK_MAGNET_CORE = new MagnetRepairCoreItem(Main.id("weak_magnet_core"), new Item.Properties().tab(HARDCORE_MOD_GROUP).stacksTo(1).setNoRepair());
    public static MagnetRepairCoreItem STRONG_MAGNET_CORE = new MagnetRepairCoreItem(Main.id("strong_magnet_core"), new Item.Properties().tab(HARDCORE_MOD_GROUP).stacksTo(1).setNoRepair());
    public static MagnetRepairCoreItem JUST_ANOTHER_MAGNET_CORE = new MagnetRepairCoreItem(Main.id("just_another_magnet_core"), new Item.Properties().tab(HARDCORE_MOD_GROUP).stacksTo(1).setNoRepair());
    public static Item ULTRA_HELMET = new CustomArmourItem(Main.id("ultra_helmet"), CustomArmorMaterial.ULTRA, EquipmentSlotType.HEAD, new Item.Properties().stacksTo(1).setNoRepair().tab(HARDCORE_MOD_GROUP));
    public static Item ULTRA_CHESTPLATE = new CustomArmourItem(Main.id("ultra_chestplate"), CustomArmorMaterial.ULTRA, EquipmentSlotType.CHEST, new Item.Properties().stacksTo(1).setNoRepair().tab(HARDCORE_MOD_GROUP));
    public static Item ULTRA_LEGGINGS = new CustomArmourItem(Main.id("ultra_leggings"), CustomArmorMaterial.ULTRA, EquipmentSlotType.LEGS, new Item.Properties().stacksTo(1).setNoRepair().tab(HARDCORE_MOD_GROUP));
    public static Item ULTRA_BOOTS = new CustomArmourItem(Main.id("ultra_boots"), CustomArmorMaterial.ULTRA, EquipmentSlotType.FEET, new Item.Properties().stacksTo(1).setNoRepair().tab(HARDCORE_MOD_GROUP));
    public static RushPaxelItem RUSH_PAXEL = new RushPaxelItem(new PaxelUltraTier(), p -> p.tab(HARDCORE_MOD_GROUP), Main.id("paxel_rush"));
    public static ReachPaxelItem REACH_PAXEL = new ReachPaxelItem(new PaxelUltraTier(), p -> p.tab(HARDCORE_MOD_GROUP), Main.id("paxel_reach"));
    public static FortunatePaxelItem FORTUNE_PAXEL = new FortunatePaxelItem(new PaxelUltraTier(), p -> p.tab(HARDCORE_MOD_GROUP), Main.id("paxel_fortunate"));
    public static PaxelItem BASIC_PAXEL = new PaxelItem(new PaxelUltraTier(), p -> p.tab(HARDCORE_MOD_GROUP), Main.id("paxel"));
    public static UnidentifiedPaxelItem UNIDENTIFIED_PAXEL = new UnidentifiedPaxelItem(Main.id("unidentified_paxel"), new Item.Properties().tab(HARDCORE_MOD_GROUP).stacksTo(1), true, "Right click to identify!", TextFormatting.AQUA);
    public static SwordItem ANOTHER_SWORD = new SwordItem(Main.id("another_sword"), new UltraTier(), 10, 0, new Item.Properties().tab(HARDCORE_MOD_GROUP).setNoRepair().fireResistant());
    public static BasicItem SPAWNER_DUST = new BasicItem(Main.id("spawner_dust"), new Item.Properties().tab(HARDCORE_MOD_GROUP).stacksTo(64), false, "", TextFormatting.WHITE);
    public static BasicItem SPAWNER_PIECE = new BasicItem(Main.id("spawner_piece"), new Item.Properties().tab(HARDCORE_MOD_GROUP).stacksTo(64), false, "", TextFormatting.WHITE);
    public static BasicItem ULTRA_NUGGET = new BasicItem(Main.id("ultra_nugget"), new Item.Properties().tab(HARDCORE_MOD_GROUP), false, "", TextFormatting.WHITE);
    public static BasicItem ULTRA_SCRAP = new BasicItem(Main.id("ultra_scrap"), new Item.Properties().tab(HARDCORE_MOD_GROUP), false, "", TextFormatting.WHITE);
    public static BasicItem ULTRA_INGOT = new BasicItem(Main.id("ultra_ingot"), new Item.Properties().tab(HARDCORE_MOD_GROUP), false, "", TextFormatting.WHITE);
    public static ItemGem RUBY_GEM = new ItemGem(HARDCORE_MOD_GROUP, 64, Main.id("gem_ruby"), new Item.Properties().fireResistant());
    public static ItemGem CRYSTAL_GEM = new ItemGem(HARDCORE_MOD_GROUP, 64, Main.id("gem_crystal"), new Item.Properties().fireResistant());
    public static BasicItem MAGNETITE = new BasicItem(Main.id("magnetite"), new Item.Properties().tab(HARDCORE_MOD_GROUP), true, "Get from Ultra Boxes!", TextFormatting.BLUE);
    public static BasicItem ARMOR_PLATING = new BasicItem(Main.id("armor_plating"), new Item.Properties().tab(HARDCORE_MOD_GROUP).fireResistant(), true, "Get from Ultra Boxes!", TextFormatting.BLUE);
    public static ItemGem GEM_IOLITE = new ItemGem(HARDCORE_MOD_GROUP, 64, Main.id("gem_iolite"), new Item.Properties());
    public static ItemGem GEM_SAPPHIRE = new ItemGem(HARDCORE_MOD_GROUP, 64, Main.id("gem_sapphire"), new Item.Properties());
    public static ItemGem GEM_TOPAZ = new ItemGem(HARDCORE_MOD_GROUP, 64, Main.id("gem_topaz"), new Item.Properties());
    public static ItemGem POG = new ItemGem(HARDCORE_MOD_GROUP, 64, Main.id("gem_pog"), new Item.Properties());
    public static PaxelCharmItem PAXEL_CHARM = new PaxelCharmItem(Main.id("paxel_charm"), new Item.Properties().tab(HARDCORE_MOD_GROUP), 1);
    public static BasicItem WOOD_MEDAL = new BasicItem(Main.id("wooden_medal"), new Item.Properties().stacksTo(16).tab(HARDCORE_MOD_GROUP), true, "Level 1 Medal", TextFormatting.BLUE);
    public static BasicItem BRONZE_MEDAL = new BasicItem(Main.id("bronze_medal"), new Item.Properties().stacksTo(16).tab(HARDCORE_MOD_GROUP), true, "Level 2 Medal", TextFormatting.BLUE);
    public static BasicItem SILVER_MEDAL = new BasicItem(Main.id("silver_medal"), new Item.Properties().stacksTo(16).tab(HARDCORE_MOD_GROUP), true, "Level 3 Medal", TextFormatting.BLUE);
    public static BasicItem GOLD_MEDAL = new BasicItem(Main.id("gold_medal"), new Item.Properties().stacksTo(16).tab(HARDCORE_MOD_GROUP), true, "Level 4 Medal", TextFormatting.BLUE);
    public static BasicItem ULTRA_MEDAL = new BasicItem(Main.id("ultra_medal"), new Item.Properties().stacksTo(16).tab(HARDCORE_MOD_GROUP), true, "Level 5 Medal (Highest Level)", TextFormatting.BLUE);
    public static UltraPearlItem ULTRA_PEARL = new UltraPearlItem(Main.id("ultra_pearl"), new Item.Properties().tab(HARDCORE_MOD_GROUP), 20);
    public static InfiniteWaterBucket INFINITE_BUCKET = new InfiniteWaterBucket(Main.id("infinite_water"), new Item.Properties().tab(HARDCORE_MOD_GROUP).stacksTo(1), () -> Fluids.WATER);
    public static InfiniteWaterBucket INFINITE_LAVA_BUCKET = new InfiniteWaterBucket(Main.id("infinite_lava"), new Item.Properties().tab(HARDCORE_MOD_GROUP).stacksTo(1), () -> Fluids.LAVA);

    public static void registerItems(RegistryEvent.Register<Item> event) {
        IForgeRegistry<Item> registry = event.getRegistry();
        registry.register(POG);
        registry.register(ULTRA_BOX);
        registry.register(ULTRA_SCRAP);
        registry.register(ULTRA_INGOT);
        registry.register(HARDCORE_DIAMOND);
        registry.register(ULTRA_NUGGET);
        registry.register(ULTRA_HELMET);
        registry.register(ULTRA_CHESTPLATE);
        registry.register(ULTRA_LEGGINGS);
        registry.register(ULTRA_BOOTS);
        registry.register(WEAK_MAGNET);
        registry.register(STRONG_MAGNET);
        registry.register(JUST_ANOTHER_MAGNET);
        registry.register(WEAK_MAGNET_CORE);
        registry.register(STRONG_MAGNET_CORE);
        registry.register(JUST_ANOTHER_MAGNET_CORE);
        registry.register(ULTRA_PEARL);
        registry.register(BASIC_PAXEL);
        registry.register(UNIDENTIFIED_PAXEL);
        registry.register(RUSH_PAXEL);
        registry.register(REACH_PAXEL);
        registry.register(FORTUNE_PAXEL);
        registry.register(PAXEL_CHARM);
        registry.register(SPAWNER_DUST);
        registry.register(SPAWNER_PIECE);
        registry.register(RUBY_GEM);
        registry.register(CRYSTAL_GEM);
        registry.register(GEM_IOLITE);
        registry.register(GEM_SAPPHIRE);
        registry.register(GEM_TOPAZ);
        registry.register(MAGNETITE);
        registry.register(ARMOR_PLATING);
        registry.register(ANOTHER_SWORD);
        registry.register(new BlockItem(Blocks.SPAWNER, new Item.Properties().tab(ItemGroup.TAB_DECORATIONS).rarity(Rarity.EPIC)).setRegistryName(Blocks.SPAWNER.getRegistryName()));
        registry.register(WOOD_MEDAL);
        registry.register(BRONZE_MEDAL);
        registry.register(SILVER_MEDAL);
        registry.register(GOLD_MEDAL);
        registry.register(ULTRA_MEDAL);
        registry.register(INFINITE_BUCKET);
//        registry.register(INFINITE_LAVA_BUCKET);
        Main.LOGGER.info("HardcoreMod Items successfully registered!");
    }

}
