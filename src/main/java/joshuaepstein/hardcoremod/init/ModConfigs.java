package joshuaepstein.hardcoremod.init;

import joshuaepstein.hardcoremod.Main;
import joshuaepstein.hardcoremod.config.*;
import joshuaepstein.hardcoremod.item.MagnetItem;

public class ModConfigs {


    public static UltraBoxConfig ULTRA_BOX;
    public static OverLevelEnchantConfig OVERLEVEL_ENCHANT;

    public static void register() {
        ULTRA_BOX = (UltraBoxConfig) new UltraBoxConfig().readConfig();
        OVERLEVEL_ENCHANT = (OverLevelEnchantConfig) new OverLevelEnchantConfig().readConfig();
        Main.LOGGER.info("HardcoreMod Configs are loaded successfully!");
    }

}
