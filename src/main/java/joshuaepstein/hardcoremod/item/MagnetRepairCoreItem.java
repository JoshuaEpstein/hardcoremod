package joshuaepstein.hardcoremod.item;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;

import java.util.List;

public class MagnetRepairCoreItem extends Item{
    public MagnetRepairCoreItem(ResourceLocation id, Item.Properties properties) {
        super(properties);

        this.setRegistryName(id);
    }

    @Override
    public void onCraftedBy(ItemStack stack, World world, PlayerEntity player) {
        CompoundNBT nbt;
        if(stack.hasTag()){
            nbt = stack.getTag();
            nbt.putString("hardcoremod:crafted_by", player.getScoreboardName());
        } else {
            nbt = new CompoundNBT();
        }
        stack.setTag(nbt);
    }

    @Override
    public void appendHoverText(ItemStack stack, World world, List<ITextComponent> tooltip, ITooltipFlag advanced) {
        CompoundNBT nbt;
        if(stack.hasTag()){
            nbt = stack.getTag();
        } else {
            nbt = new CompoundNBT();
        }
        if(nbt.contains("hardcoremod:crafted_by")){
            tooltip.add(new StringTextComponent(TextFormatting.GOLD + "Crafted by: " + TextFormatting.GRAY + nbt.getString("hardcoremod:crafted_by")));
        }
    }

}
