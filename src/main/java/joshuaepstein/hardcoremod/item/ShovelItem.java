package joshuaepstein.hardcoremod.item;

import net.minecraft.item.IItemTier;
import net.minecraft.util.ResourceLocation;

import net.minecraft.item.Item.Properties;

public class ShovelItem extends net.minecraft.item.ShovelItem {

   public ShovelItem(ResourceLocation id, IItemTier tier, int attackDamageIn, float attackSpeedIn, Properties builder) {
      super(tier, attackDamageIn, attackSpeedIn, builder);
      this.setRegistryName(id);
   }
}