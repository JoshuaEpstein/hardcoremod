package joshuaepstein.hardcoremod.item;

import joshuaepstein.hardcoremod.Main;
import joshuaepstein.hardcoremod.init.ModAttributes;
import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.IArmorMaterial;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.*;
import net.minecraft.world.World;
import javax.annotation.Nullable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public class CustomArmourItem extends ArmorItem {
    public CustomArmourItem(ResourceLocation id, IArmorMaterial armorMaterial, EquipmentSlotType equipmentSlotType, Item.Properties properties){
        super(armorMaterial, equipmentSlotType, properties);
        this.setRegistryName(id);
    }

    @Override
    public boolean canDisableShield(ItemStack stack, ItemStack shield, LivingEntity entity, LivingEntity attacker) {
        CompoundNBT nbt;
        if(stack.hasTag()) {
            nbt = stack.getTag();
            if(nbt.getInt(Main.sId("disable_shield")) == 1){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    @Override
    public void onCraftedBy(ItemStack stack, World world, PlayerEntity player) {
        CompoundNBT nbt = null;
        if(stack.hasTag()){
            nbt = stack.getTag();
            nbt.putString("hardcoremod:crafted_by", player.getScoreboardName());
        }
        stack.setTag(nbt);
        ModAttributes.MAX_REPAIRS.getOrCreate(stack, Main.getRandomNumber(3, 6));
    }
    @Override
    public void onArmorTick(ItemStack stack, World world, PlayerEntity player) {
        super.onArmorTick(stack, world, player);
    }

    @Override
    public void appendHoverText(ItemStack stack, @Nullable World world, List<ITextComponent> tooltip, ITooltipFlag advanced) {
        CompoundNBT nbt = null;
        if(stack.hasTag()){
            nbt = stack.getOrCreateTag();
            if(nbt.contains("hardcoremod:crafted_by")){
                tooltip.add(new StringTextComponent(TextFormatting.GOLD + "Crafted by: " + TextFormatting.GRAY + nbt.getString("hardcoremod:crafted_by")));
            }

        }

        //Attributes
        ModAttributes.MAX_REPAIRS.get(stack).map(attribute -> attribute.getValue(stack)).ifPresent(value -> {
            int current = ModAttributes.CURRENT_REPAIRS.getOrDefault(stack, 0).getValue(stack);
            int unfilled = value - current;
            tooltip.add(new StringTextComponent("Repairs: ")
                    .append(tooltipDots(current, TextFormatting.YELLOW))
                    .append(tooltipDots(unfilled, TextFormatting.GRAY)));
        });
        nbt.putInt("hardcoremod:added_plating", 1);
        if(nbt.getInt("hardcoremod:added_plating") != 1){
            tooltip.add(new StringTextComponent(TextFormatting.LIGHT_PURPLE + "+" + (nbt.getInt("hardcoremod:added_plating")-1) + " Plating"));
        }
        stack.setTag(nbt);
    }
    static ITextComponent tooltipDots(int amount, TextFormatting formatting) {
        StringBuilder text = new StringBuilder();
        for (int i = 0; i < amount; i++) {
            text.append("\u2b22 ");
        }
        return new StringTextComponent(text.toString()).withStyle(formatting);
    }
    @Nullable
    @Override
    public <A extends BipedModel<?>> A getArmorModel(LivingEntity entityLiving, ItemStack stack, EquipmentSlotType armorSlot, A _default) {
       if(!stack.isEmpty()){
            if(stack.getItem() instanceof CustomArmourItem){
                return null;
            }
       }
       return null;
    }
    static String format(double value, int scale) {
        return BigDecimal.valueOf(value).setScale(scale, RoundingMode.HALF_UP).stripTrailingZeros().toPlainString();
    }
}
