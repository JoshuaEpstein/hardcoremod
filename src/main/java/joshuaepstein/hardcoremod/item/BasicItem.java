package joshuaepstein.hardcoremod.item;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;

import java.util.List;

import net.minecraft.item.Item.Properties;

public class BasicItem extends Item {
    boolean tooltips = false;
    String msg = "";
    TextFormatting color = TextFormatting.WHITE;
    public BasicItem(ResourceLocation id, Properties properties, boolean tooltip, String tooltipMSG, TextFormatting tooltipColour) {
        super(properties);
        tooltips = tooltip;
        msg = tooltipMSG;
        if(tooltipColour != TextFormatting.WHITE){
            color = tooltipColour;
        }
        this.setRegistryName(id);
    }
    @Override
    public void appendHoverText(ItemStack stack, World world, List<ITextComponent> tooltip, ITooltipFlag advanced) {
    if(tooltips){
        tooltip.add(new StringTextComponent(color + msg));
    }
    }
}
