package joshuaepstein.hardcoremod.item;

import joshuaepstein.hardcoremod.Main;
import net.minecraft.block.Block;
import net.minecraft.item.*;
import net.minecraft.tags.ITag;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.Tag;
import net.minecraft.util.*;

import net.minecraft.item.Item.Properties;
import net.minecraftforge.common.ToolType;

public class ItemGem extends Item {
    public ItemGem(ItemGroup group, int maxStackSize, ResourceLocation id, Properties properties){
        super(properties.tab(group).stacksTo(maxStackSize));
        this.setRegistryName(id);
    }
}
