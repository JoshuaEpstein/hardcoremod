package joshuaepstein.hardcoremod.item;

import net.minecraft.entity.*;
import net.minecraft.util.*;

import net.minecraft.item.Item.Properties;

public class SpawnEggItem extends net.minecraft.item.SpawnEggItem {


    public SpawnEggItem(ResourceLocation id, Properties properties, EntityType<?> typeIn, int primaryColorIn, int secondaryColorIn) {
        super(typeIn, primaryColorIn, secondaryColorIn, properties);

        this.setRegistryName(id);
    }
}
