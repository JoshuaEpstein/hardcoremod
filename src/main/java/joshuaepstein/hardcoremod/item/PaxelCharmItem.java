package joshuaepstein.hardcoremod.item;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;

import javax.xml.soap.Text;
import java.util.List;

public class PaxelCharmItem extends BasicItem {
    public PaxelCharmItem(ResourceLocation id, Item.Properties properties, int stackSize){
        super(id, properties.stacksTo(stackSize), false, "Combine with Paxel to get a random enchantment on it.", TextFormatting.BLUE);
    }
    @Override
    public void appendHoverText(ItemStack stack, World world, List<ITextComponent> tooltip, ITooltipFlag advanced){
        tooltip.add(new StringTextComponent(TextFormatting.AQUA + "Apply with a " + TextFormatting.BLUE + "Paxel" + TextFormatting.AQUA + " to get a random enchantment"));
    }
}
