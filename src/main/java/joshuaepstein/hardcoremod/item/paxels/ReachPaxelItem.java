package joshuaepstein.hardcoremod.item.paxels;

import com.google.common.collect.*;
import joshuaepstein.hardcoremod.init.ModItems;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.material.Material;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.ai.attributes.Attribute;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.*;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeMod;
import net.minecraftforge.common.ToolType;
import net.minecraftforge.fml.RegistryObject;

import java.util.*;
import java.util.function.Function;

public class ReachPaxelItem extends ToolItem {
    private static final Set<Material> EFFECTIVE_ON_MATERIALS = Sets.newHashSet(Material.WOOD, Material.NETHER_WOOD, Material.PLANT, Material.REPLACEABLE_PLANT, Material.BAMBOO, Material.VEGETABLE);
    private static final Map<Block, BlockState> PATH_STUFF = Maps.newHashMap(ImmutableMap.of(Blocks.GRASS_BLOCK, Blocks.GRASS_PATH.defaultBlockState()));
    protected final float speed;
    protected static final UUID REACH_UUID = UUID.fromString("e6a80303-d741-427f-81cc-c2c1d9cdc66d");
    private final float attackDamageBaseline;
    private Multimap<Attribute, AttributeModifier> defaultModifiers;
    public ReachPaxelItem(IItemTier tier, Function<Properties, Properties> properties, ResourceLocation id) {
        super(4.0F, 0.3F, tier, new HashSet<>(), properties.apply(new Properties()
                .defaultDurability((int) (tier.getUses() * 1.5))
                .addToolType(ToolType.PICKAXE, tier.getLevel())
                .addToolType(ToolType.SHOVEL, tier.getLevel())
                .addToolType(ToolType.AXE, tier.getLevel())
        ));
        this.setRegistryName(id);
        this.speed = tier.getSpeed();
        this.attackDamageBaseline = 4.0F + tier.getAttackDamageBonus();

    }

    @Override
    public void fillItemCategory(ItemGroup group, NonNullList<ItemStack> items) {
        if (this.isEnabled()) {
            super.fillItemCategory(group, items);
        }
    }

    @Override
    public boolean isCorrectToolForDrops(BlockState state) {
        int i = this.getTier().getLevel();
        if (state.getHarvestTool() == ToolType.PICKAXE)
            return i >= state.getHarvestLevel();

        Material material = state.getMaterial();
        return material == Material.STONE || material == Material.METAL || material == Material.HEAVY_METAL
                || state.is(Blocks.SNOW) || state.is(Blocks.SNOW_BLOCK);
    }

    @Override
    public float getDestroySpeed(ItemStack stack, BlockState state) {
        Material material = state.getMaterial();
            return material != Material.METAL && material != Material.HEAVY_METAL && material != Material.STONE
                    && !EFFECTIVE_ON_MATERIALS.contains(material)
                    ? super.getDestroySpeed(stack, state)
                    : this.speed;

    }


    @Override
    public ActionResultType useOn(ItemUseContext context) {
        World world = context.getLevel();
        BlockPos pos = context.getClickedPos();
        PlayerEntity player = context.getPlayer();
        ItemStack stack = context.getItemInHand();

        BlockState state = world.getBlockState(pos);
        BlockState modifiedState = state.getToolModifiedState(world, pos, player, stack, ToolType.AXE);

        if (modifiedState != null) {
            world.playSound(player, pos, SoundEvents.AXE_STRIP, SoundCategory.BLOCKS, 1.0F, 1.0F);

            if (!world.isClientSide()) {
                world.setBlock(pos, modifiedState, 11);

                if (player != null) {
                    stack.hurtAndBreak(1, player, entity -> {
                        entity.broadcastBreakEvent(context.getHand());
                    });
                }
            }

            return ActionResultType.sidedSuccess(world.isClientSide());
        } else if (context.getClickedFace() != Direction.DOWN && world.getBlockState(pos.above()).isAir(world, pos.above())) {
            BlockState pathState = PATH_STUFF.get(state.getBlock());
            if (pathState != null) {
                world.playSound(player, pos, SoundEvents.SHOVEL_FLATTEN, SoundCategory.BLOCKS, 1.0F, 1.0F);

                if (!world.isClientSide()) {
                    world.setBlock(pos, pathState, 11);

                    if (player != null) {
                        stack.hurtAndBreak(1, player, entity -> {
                            entity.broadcastBreakEvent(context.getHand());
                        });
                    }
                }

                return ActionResultType.SUCCESS;
            }
        }

        return ActionResultType.PASS;
    }
    public boolean isEnabled() {
        return true;
    }

    @Override
    public void appendHoverText(ItemStack stack, World world, List<ITextComponent> tooltip, ITooltipFlag advanced) {
        tooltip.add(new StringTextComponent(TextFormatting.GRAY + "Limited Repairs: Disabled"));
        
        CompoundNBT nbt = null;
        if(stack.hasTag()){
            nbt = stack.getOrCreateTag();
        }
        if(!nbt.getBoolean("hardcoremod:added_charm_paxel")) {
            tooltip.add(new StringTextComponent(TextFormatting.GRAY + "Combine with Paxel Charm to enable the enchantment!"));
        }
        if(nbt.getBoolean("hardcoremod:added_charm_paxel")) {
            tooltip.add(new StringTextComponent(TextFormatting.GRAY + "This Paxel has " + TextFormatting.GREEN + "Reach"));
            tooltip.add(new StringTextComponent(TextFormatting.GRAY + "      Adds Reach " + TextFormatting.GREEN + "(II)"));
            if(Screen.hasControlDown()){
                tooltip.add(new StringTextComponent(TextFormatting.GRAY + "Wow! You found a secret text! Hold Alt to find another secret!"));
            }
            if(Screen.hasAltDown()){
                tooltip.add(new StringTextComponent(TextFormatting.GRAY + "Another secret text! I wonder what this text will be in the next update?"));
            }
        }
        if(nbt.contains("hardcoremod:crafted_by")){
            tooltip.add(new StringTextComponent(TextFormatting.GOLD + "Crafted by: " + TextFormatting.GRAY + nbt.getString("hardcoremod:crafted_by")));
        }
    }

    @Override
    public void onCraftedBy(ItemStack stack, World world, PlayerEntity player) {
        CompoundNBT nbt = null;
        if(stack.hasTag()){
            nbt = stack.getOrCreateTag();
            nbt.putString("hardcoremod:crafted_by", player.getScoreboardName());
        }
        stack.setTag(nbt);
    }

    @Override
    public Multimap<Attribute, AttributeModifier> getAttributeModifiers(EquipmentSlotType slot, ItemStack stack) {
        ImmutableMultimap.Builder<Attribute, AttributeModifier> builder = ImmutableMultimap.builder();
        builder.put(Attributes.ATTACK_DAMAGE, new AttributeModifier(BASE_ATTACK_DAMAGE_UUID, "Tool modifier", (double) this.attackDamageBaseline, AttributeModifier.Operation.ADDITION));
        builder.put(Attributes.ATTACK_SPEED, new AttributeModifier(BASE_ATTACK_SPEED_UUID, "Tool modifier", (double) 0.3F, AttributeModifier.Operation.ADDITION));
        if(stack.getOrCreateTag().getInt("hardcoremod:added_charm_paxel") == 1) {
            builder.put(ForgeMod.REACH_DISTANCE.get(), new AttributeModifier(REACH_UUID, "Tool Addon", 2F, AttributeModifier.Operation.ADDITION));
        }
        this.defaultModifiers = builder.build();
        return slot == EquipmentSlotType.MAINHAND ? this.defaultModifiers : super.getAttributeModifiers(slot, stack);
    }

}
