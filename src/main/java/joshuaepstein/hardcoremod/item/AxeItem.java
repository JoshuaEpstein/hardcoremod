package joshuaepstein.hardcoremod.item;

import net.minecraft.item.IItemTier;
import net.minecraft.util.ResourceLocation;

import net.minecraft.item.Item.Properties;

public class AxeItem extends net.minecraft.item.AxeItem {

   public AxeItem(ResourceLocation id, IItemTier tier, int attackDamageIn, float attackSpeedIn, Properties builder) {
      super(tier, attackDamageIn, attackSpeedIn, builder);
      this.setRegistryName(id);
   }
}
