package joshuaepstein.hardcoremod.item;

import net.minecraft.item.IItemTier;
import net.minecraft.util.ResourceLocation;

import net.minecraft.item.Item.Properties;

public class HoeItem extends net.minecraft.item.HoeItem {

   public HoeItem(ResourceLocation id, IItemTier tier, int attackDamageIn, float attackSpeedIn, Properties builder) {
      super(tier, attackDamageIn, attackSpeedIn, builder);
      this.setRegistryName(id);
   }
}