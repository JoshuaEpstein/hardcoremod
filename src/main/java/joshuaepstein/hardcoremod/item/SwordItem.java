package joshuaepstein.hardcoremod.item;

import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.ImmutableMultimap.Builder;
import net.minecraft.entity.ai.attributes.Attribute;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.item.IItemTier;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;

public class SwordItem extends net.minecraft.item.SwordItem {
   private final float attackDamage;
   public SwordItem(ResourceLocation id, IItemTier ItemTier, int attackDamage, float attackSpeed, Item.Properties p_i48460_4_) {
      super(ItemTier, attackDamage, attackSpeed, p_i48460_4_);
      this.attackDamage = (float)attackDamage + ItemTier.getAttackDamageBonus();
      Builder<Attribute, AttributeModifier> builder = ImmutableMultimap.builder();
      builder.put(Attributes.ATTACK_DAMAGE, new AttributeModifier(BASE_ATTACK_DAMAGE_UUID, "Weapon modifier", (double)this.attackDamage, AttributeModifier.Operation.ADDITION));
      builder.put(Attributes.ATTACK_SPEED, new AttributeModifier(BASE_ATTACK_SPEED_UUID, "Weapon modifier", (double)attackSpeed, AttributeModifier.Operation.ADDITION));
      this.setRegistryName(id);
   }
}
