package joshuaepstein.hardcoremod.item;

import joshuaepstein.hardcoremod.helper.NBTHelper;
import joshuaepstein.hardcoremod.init.ModConfigs;
import joshuaepstein.hardcoremod.init.ModItems;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.ExperienceOrbEntity;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;

import javax.annotation.Nullable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class MagnetItem extends Item {
    private int magnetRange;
    private int maxDurability = 0;

    public MagnetItem(ResourceLocation id, Properties properties, int MagnetRange, int maxDura) {
        super(properties.durability(maxDura));
        magnetRange = MagnetRange;
        maxDurability = maxDura;
        this.setRegistryName(id);

    }

    @Override
    public boolean isFoil(ItemStack stack) {
        return NBTHelper.getBoolean(stack, "Enabled");
    }

    @Override
    public ActionResult<ItemStack> use(World world, PlayerEntity player, Hand hand) {
        ItemStack stack = player.getItemInHand(hand);
//        player.playSound(SoundEvents.EXPERIENCE_ORB_PICKUP, 0.5F, NBTHelper.getBoolean(stack, "Enabled") ? 0.5F : 1.0F);
        NBTHelper.flipBoolean(stack, "Enabled");
        if(stack.getDamageValue() == maxDurability && NBTHelper.getBoolean(stack, "Enabled")){
            NBTHelper.flipBoolean(stack, "Enabled");
        }
        return new ActionResult<>(ActionResultType.SUCCESS, stack);
    }
    @Override
    public void appendHoverText(ItemStack stack, World world, List<ITextComponent> tooltip, ITooltipFlag advanced) {
        CompoundNBT nbt = null;
        if(stack.hasTag()){
            nbt = stack.getTag();
        } else {
            nbt = new CompoundNBT();
        }
        if (NBTHelper.getBoolean(stack, "Enabled")) {
            tooltip.add(new StringTextComponent("Enabled: \u00A7a\u00A7ltrue"));
        } else {
            tooltip.add(new StringTextComponent("Enabled: \u00A7c\u00A7lfalse"));
        }
        tooltip.add(new StringTextComponent("  - Range: \u00A76" + magnetRange));
        tooltip.add(new StringTextComponent(TextFormatting.BLUE + "Hold Shift to view more info"));
        if(Screen.hasShiftDown()){
            if(nbt.contains("hardcoremod:crafted_by")) {
                tooltip.add(new StringTextComponent(TextFormatting.GOLD + "Crafted By: " + TextFormatting.GRAY + nbt.getString("hardcoremod:crafted_by")));
            } else {
                tooltip.add(new StringTextComponent(TextFormatting.GRAY + "No Crafting Information..."));
            }
        }
    }
    @Override
    public void inventoryTick(ItemStack stack, World world, Entity entity, int slot, boolean isSelected) {
        if (entity instanceof PlayerEntity && NBTHelper.getBoolean(stack, "Enabled")) {
            double range = magnetRange;
            List<ItemEntity> items = world.getEntitiesOfClass(ItemEntity.class, entity.getBoundingBox().inflate(range));
            PlayerEntity player = (PlayerEntity) entity;
            for (ItemEntity item : items) {
                if (!item.isAlive() || NBTHelper.getBoolean(stack, "PreventRemoteMovement"))
                    continue;
                item.setNoPickUpDelay();
//                if (item.getThrower() != null && item.getThrower().equals(entity.getUUID()) && item.hasPickUpDelay())
//                    continue;
                if(item.getX() == entity.getX() && item.getY() == entity.getY() && item.getZ() == entity.getZ()){
                    return;
                }
                if (!world.isClientSide()) {
                    item.setNoPickUpDelay();
                    if(!this.fullInventory(player, stack)){
                        stack.setDamageValue(stack.getDamageValue() + 1);
                    }
                    item.setPos(entity.getX(), entity.getY(), entity.getZ());
                    if(stack.getDamageValue() == maxDurability){
                        NBTHelper.flipBoolean(stack, "Enabled");
                        return;
                    }
                }
            }
            List<ExperienceOrbEntity> xporbs = world.getEntitiesOfClass(ExperienceOrbEntity.class, entity.getBoundingBox().inflate(range));
            for (ExperienceOrbEntity orb : xporbs) {
                if (!world.isClientSide()) {
                    orb.setPos(entity.getX(), entity.getY(), entity.getZ());
                    if(!this.fullInventory(player, stack)){
                        stack.setDamageValue(stack.getDamageValue() + 1);
                    }
                }
            }
        }
    }

    public boolean fullInventory(PlayerEntity player, ItemStack stack){
        for(int invslot = 0; invslot < player.inventory.items.size(); invslot++){
            if (player.inventory.items.get(invslot).isEmpty()){
                return false;
            }
        }
        return true;
    }

    @Override
    public void onCraftedBy(ItemStack stack, @Nullable World world, @Nullable PlayerEntity playerEntity) {
        CompoundNBT nbt = null;
        if(stack.hasTag()){
            nbt = stack.getOrCreateTag();
        }
        nbt.putString("hardcoremod:crafted_by", playerEntity.getScoreboardName());
        stack.setTag(nbt);
    }
    @Override
    public boolean isRepairable(ItemStack stack) {
        return false;
    }
}