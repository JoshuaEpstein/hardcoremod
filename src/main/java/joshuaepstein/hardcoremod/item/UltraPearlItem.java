package joshuaepstein.hardcoremod.item;

import net.minecraft.entity.item.EnderPearlEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.EnderPearlItem;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.Stats;
import net.minecraft.util.*;
import net.minecraft.world.World;

public class UltraPearlItem extends EnderPearlItem {

    public UltraPearlItem(ResourceLocation id, Properties properties, int maxDamage) {
        super(properties.durability(maxDamage));
        this.setRegistryName(id);
    }

    @Override
    public ActionResult<ItemStack> use(World p_77659_1_, PlayerEntity p_77659_2_, Hand p_77659_3_) {
        ItemStack itemstack = p_77659_2_.getItemInHand(p_77659_3_);
        p_77659_1_.playSound((PlayerEntity)null, p_77659_2_.getX(), p_77659_2_.getY(), p_77659_2_.getZ(), SoundEvents.ENDER_PEARL_THROW, SoundCategory.NEUTRAL, 0.5F, 0.4F / (random.nextFloat() * 0.4F + 0.8F));
        p_77659_2_.getCooldowns().addCooldown(this, 20);
        if((itemstack.getMaxDamage() - itemstack.getDamageValue()) < 2){
            itemstack.shrink(1);
            return ActionResult.pass(itemstack);
        }
        if (!p_77659_1_.isClientSide) {
            EnderPearlEntity enderpearlentity = new EnderPearlEntity(p_77659_1_, p_77659_2_);
            enderpearlentity.setItem(itemstack);
            enderpearlentity.shootFromRotation(p_77659_2_, p_77659_2_.xRot, p_77659_2_.yRot, 0.0F, 1.5F, 1.0F);
            p_77659_1_.addFreshEntity(enderpearlentity);
        }

        p_77659_2_.awardStat(Stats.ITEM_USED.get(this));

        if (!p_77659_2_.abilities.instabuild) {
            itemstack.setDamageValue(itemstack.getDamageValue() + 1);
        }

        return ActionResult.sidedSuccess(itemstack, p_77659_1_.isClientSide());
    }

    @Override
    public boolean isEnchantable(ItemStack p_77616_1_) {
        return false;
    }
}