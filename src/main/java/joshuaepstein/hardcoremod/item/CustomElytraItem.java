package joshuaepstein.hardcoremod.item;

import joshuaepstein.hardcoremod.init.*;
import net.minecraft.item.*;
import net.minecraft.util.*;

import net.minecraft.item.Item.Properties;

public class CustomElytraItem extends ElytraItem {
    public CustomElytraItem(ResourceLocation id, Properties properties) {
        super(properties);

        this.setRegistryName(id);
    }

    @Override
    public boolean isValidRepairItem(ItemStack toRepair, ItemStack repair) {
        return repair.getItem() == ModItems.HARDCORE_DIAMOND;

    }

}
