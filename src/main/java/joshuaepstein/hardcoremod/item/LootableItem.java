package joshuaepstein.hardcoremod.item;

import joshuaepstein.hardcoremod.init.*;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.particles.*;
import net.minecraft.util.*;
import net.minecraft.util.math.vector.*;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraft.world.server.*;

import java.util.function.Supplier;

public class LootableItem extends BasicItem {

    private final Supplier<ItemStack> supplier;

    public LootableItem(ResourceLocation id, Item.Properties properties, Supplier<ItemStack> supplier, boolean enableTooltip, String tooltipMsg, TextFormatting tooltipColour) {
        super(id, properties, enableTooltip, tooltipMsg, tooltipColour);
        this.supplier = supplier;
    }
    public static void successEffects(World world, Vector3d position) {
        world.playSound(
                null,
                position.x,
                position.y,
                position.z,
                ModSounds.SUCCESS_SFX,
                SoundCategory.PLAYERS,
                1f, 1f
        );

        ((ServerWorld) world).sendParticles(ParticleTypes.DRAGON_BREATH,
                position.x,
                position.y,
                position.z,
                500,
                1, 1, 1,
                0.5
        );
    }
    @Override
    public ActionResult<ItemStack> use(World world, PlayerEntity player, Hand hand) {
        if (!world.isClientSide) {
            ItemStack heldStack = player.getItemInHand(hand);
            successEffects(world, player.position());

            ItemStack randomLoot = this.supplier.get();
            while (randomLoot.getCount() > 0) {
                int amount = Math.min(randomLoot.getCount(), randomLoot.getMaxStackSize());
                ItemStack copy = randomLoot.copy();
                copy.setCount(amount);
                randomLoot.shrink(amount);
                player.drop(copy, false, false);
            }

            heldStack.shrink(1);
        }

        return super.use(world, player, hand);
    }


}
