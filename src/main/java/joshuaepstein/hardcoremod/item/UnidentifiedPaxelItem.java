package joshuaepstein.hardcoremod.item;

import com.mojang.blaze3d.matrix.MatrixStack;
import joshuaepstein.hardcoremod.Main;
import joshuaepstein.hardcoremod.client.gui.helper.ConfettiParticles;
import joshuaepstein.hardcoremod.init.ModItems;
import joshuaepstein.hardcoremod.init.ModSounds;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.SimpleSound;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

import java.util.List;

public class UnidentifiedPaxelItem extends BasicItem{

    public UnidentifiedPaxelItem(ResourceLocation id, Item.Properties properties, boolean tooltipEnabled, String tooltipMSG, TextFormatting tooltipColour){
        super(id, properties, tooltipEnabled, tooltipMSG, tooltipColour);
    }
    private static ConfettiParticles leftConfettiPopper = new ConfettiParticles()
            .angleRange(200 + 90, 265 + 90)
            .quantityRange(60, 80)
            .delayRange(0, 10)
            .lifespanRange(20, 20 * 5)
            .sizeRange(2, 5)
            .speedRange(2, 10);

    private static ConfettiParticles rightConfettiPopper = new ConfettiParticles()
            .angleRange(200, 265)
            .quantityRange(60, 80)
            .delayRange(0, 10)
            .lifespanRange(20, 20 * 5)
            .sizeRange(2, 5)
            .speedRange(2, 10);
    @Override
    public ActionResult<ItemStack> use(World world, PlayerEntity playerEntity, Hand hand) {
        int randomNumber = Main.getRandomNumber(1, 4);
        ItemStack paxel = new ItemStack(ModItems.BASIC_PAXEL);
        CompoundNBT nbt = null;
        nbt = playerEntity.getItemInHand(hand).getOrCreateTag();
        if(randomNumber == 1){
            paxel = new ItemStack(ModItems.RUSH_PAXEL);
        }
        if(randomNumber == 2){
            paxel = new ItemStack(ModItems.REACH_PAXEL);
        }
        if(randomNumber == 3){
            paxel = new ItemStack(ModItems.FORTUNE_PAXEL);
        }
        paxel.setTag(nbt);
        Minecraft.getInstance().getSoundManager().play(SimpleSound.forUI(
                ModSounds.CONFETTI_SFX,
                1.0F
        ));
        leftConfettiPopper.pop();
        rightConfettiPopper.pop();
        return ActionResult.success(paxel);
    }
    @SubscribeEvent
    public static void
    onPostRender(RenderGameOverlayEvent.Post event) {
        if (event.getType() != RenderGameOverlayEvent.ElementType.HOTBAR)
            return; // Render only on HOTBAR

        Minecraft minecraft = Minecraft.getInstance();
        MatrixStack matrixStack = event.getMatrixStack();

        int width = minecraft.getWindow().getWidth();
        int height = minecraft.getWindow().getWidth();

        int midX = width / 2;
        int midY = height / 2;

        leftConfettiPopper.spawnedPosition(
                10,
                midY
        );
        rightConfettiPopper.spawnedPosition(
                width - 10,
                midY
        );

        leftConfettiPopper.tick();
        rightConfettiPopper.tick();

        leftConfettiPopper.render(matrixStack);
        rightConfettiPopper.render(matrixStack);
    }
    @Override
    public void appendHoverText(ItemStack stack, World world, List<ITextComponent> tooltip, ITooltipFlag advanced) {
        tooltip.add(new StringTextComponent(TextFormatting.GOLD + "Crafted by: " + TextFormatting.GRAY + stack.getOrCreateTag().getString("hardcoremod:crafted_by")));
    }
}
