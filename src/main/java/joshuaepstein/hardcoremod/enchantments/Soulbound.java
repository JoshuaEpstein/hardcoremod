package joshuaepstein.hardcoremod.enchantments;

import joshuaepstein.hardcoremod.lists.EnchantmentList;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentType;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.util.Util;
import net.minecraft.util.text.*;

public class Soulbound extends Enchantment {
    public Soulbound(Rarity rarityIn, EnchantmentType typeIn, EquipmentSlotType[] slots) {
        super(rarityIn, typeIn, slots);
    }

    @Override
    public int getMaxLevel()
    {
        return 1;
    }

    @Override
    public int getMinLevel()
    {
        return 1;
    }

    @Override
    public boolean isTreasureOnly() {
        return true;
    }

    @Override
    public boolean isAllowedOnBooks() {
        return true;
    }
    public boolean curseApplicable = false;
    @Override
    public boolean checkCompatibility(Enchantment ench)
    {
        if(ench == EnchantmentList.SOULBOUND.get())
        {
            return false;
        }
        if(curseApplicable)
        {
            return true;
        }
        else return ench != Enchantments.BINDING_CURSE && ench != Enchantments.VANISHING_CURSE && ench !=Enchantments.MENDING;
    }

    @Override
    public int getMinCost(int enchantmentLevel)
    {
        return (int) ((4 * 4 * enchantmentLevel) - 3);
    }

    @Override
    public int getMaxCost(int enchantmentLevel)
    {
        return getMinCost(enchantmentLevel) + 50;
    }

    @Override
    public boolean isTradeable() {
        return true;
    }

    @Override
    public boolean isDiscoverable() {
        return true;
    }

    @Override
    public ITextComponent getFullname(int level) {
        return new StringTextComponent(TextFormatting.LIGHT_PURPLE + "Soulbound");
    }

    @Override
    protected String getOrCreateDescriptionId() {
        if (this.descriptionId == null) {
            this.descriptionId = Util.makeDescriptionId("Items that have soulbound will stay in your inventory on death.", EnchantmentList.SOULBOUND.getId());
        }

        return "Items that have soulbound will stay in your inventory on death.";
    }
}