package joshuaepstein.hardcoremod.enchantments;

import com.google.common.collect.Sets;
import joshuaepstein.hardcoremod.init.ModItems;
import joshuaepstein.hardcoremod.item.paxels.RushPaxelItem;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentType;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.*;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;

import java.util.Set;

public class SpawnerSpawnEgg extends Enchantment {
    public SpawnerSpawnEgg(Rarity rarityIn, EnchantmentType typeIn, EquipmentSlotType[] slots) {
        super(rarityIn, typeIn, slots);
    }

    @Override
    public int getMaxLevel()
    {
        return 1;
    }

    @Override
    public int getMinLevel()
    {
        return 1;
    }

    @Override
    public boolean isTreasureOnly() {
        return true;
    }

    @Override
    public boolean isAllowedOnBooks() {
        return true;
    }

    @Override
    public int getMinCost(int enchantmentLevel)
    {
        return (int) ((4 * 4 * enchantmentLevel) - 3);
    }

    @Override
    public int getMaxCost(int enchantmentLevel)
    {
        return getMinCost(enchantmentLevel) + 50;
    }

    @Override
    public boolean isTradeable() {
        return true;
    }

    @Override
    public boolean isDiscoverable() {
        return true;
    }

    @Override
    public ITextComponent getFullname(int level) {
        return new StringTextComponent(TextFormatting.LIGHT_PURPLE + "Spawner: Spawn Egg Drop");
    }
    private static final Set<Item> CAN_APPLY_TO = Sets.newHashSet(Items.IRON_PICKAXE, Items.DIAMOND_PICKAXE, Items.NETHERITE_PICKAXE, ModItems.RUSH_PAXEL, ModItems.REACH_PAXEL, ModItems.BASIC_PAXEL, ModItems.FORTUNE_PAXEL);

//    @Override
//    public boolean canEnchant(ItemStack stack) {
//        return stack.getItem() instanceof RushPaxelItem || stack.getItem() instanceof PickaxeItem ? true : super.canEnchant(stack);
//    }

    @Override
    public boolean canApplyAtEnchantingTable(ItemStack stack){
        return CAN_APPLY_TO.contains(stack.getItem());
    }
}