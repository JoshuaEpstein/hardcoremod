package joshuaepstein.hardcoremod;

import org.spongepowered.asm.mixin.Mixins;
import org.spongepowered.asm.mixin.connect.IMixinConnector;

public class MixinConnector implements IMixinConnector {

	@Override
	public void connect() {
		Mixins.addConfigurations("assets/" + Main.MOD_ID + "/" + Main.MOD_ID + ".mixins.json");
	}

}
