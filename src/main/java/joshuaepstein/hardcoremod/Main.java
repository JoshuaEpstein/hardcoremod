package joshuaepstein.hardcoremod;

import joshuaepstein.hardcoremod.event.SpawnerEventHandler;
import joshuaepstein.hardcoremod.init.ModCommands;
import joshuaepstein.hardcoremod.init.ModItems;
import joshuaepstein.hardcoremod.lists.EnchantmentList;
import joshuaepstein.hardcoremod.world.OreGeneration;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegisterCommandsEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.world.BiomeLoadingEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.fml.loading.FMLEnvironment;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

@Mod(Main.MOD_ID)
public class Main {
    public static final String MOD_ID = "hardcoremod";
    public static final String MOD_VERSION = "1.2";
    public static final Logger LOGGER = LogManager.getLogger();
    public static int getRandomNumber(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }
    public static int getRandomInt(int max) {
        return (int) Math.floor(Math.random() * max);
    }
    public Main() {
        MinecraftForge.EVENT_BUS.addListener(EventPriority.NORMAL, this::onCommandRegister);
        MinecraftForge.EVENT_BUS.addListener(EventPriority.HIGH, this::onBiomeLoad);
        MinecraftForge.EVENT_BUS.addListener(EventPriority.NORMAL, this::onPlayerLoggedIn);
        LOGGER.info("[je.] Hardcore Mod Loading Version " + TextFormatting.BLUE + MOD_VERSION);
        LOGGER.info("[je.] This mod was made by JoshuaEpstein - https://joshuaepstein.co.uk");
        MinecraftForge.EVENT_BUS.addListener(EventPriority.HIGH, OreGeneration::generateOres);
        EnchantmentList.ENCHANTMENTS.register(FMLJavaModLoadingContext.get().getModEventBus());
        MinecraftForge.EVENT_BUS.register(this);
        MinecraftForge.EVENT_BUS.register(new SpawnerEventHandler());

    }
    public void onCommandRegister(RegisterCommandsEvent event) {
        ModCommands.registerCommands(event.getDispatcher(), event.getEnvironment());
    }
    public void onBiomeLoad(BiomeLoadingEvent event) {
//        event.getGeneration().withFeature(GenerationStage.Decoration.UNDERGROUND_DECORATION, ModFeatures.ORE);
    }

    public void onPlayerLoggedIn(PlayerEvent.PlayerLoggedInEvent event) {
        ServerPlayerEntity player = (ServerPlayerEntity) event.getPlayer();
//        Minecraft.getInstance().player.connection.getAdvancementManager().setSelectedTab(Minecraft.getInstance().player.connection.getAdvancementManager().getAdvancementList().getAdvancement(Registry.), );
    }

    public static String sId(String name) {
        return MOD_ID + ":" + name;
    }

    public static ResourceLocation id(String name) {
        return new ResourceLocation(MOD_ID, name);
    }

    public static List<ITextComponent> tooltipAdd(TextFormatting colour, String msg){
        List<ITextComponent> tooltip = null;
        tooltip.add(new StringTextComponent(colour + msg));
        return tooltip;
    }

    public static boolean randomChance(){
        int number = getRandomNumber(1, 2);
        if(number == 2){
            return true;
        } else {
            return false;
        }
    }

}