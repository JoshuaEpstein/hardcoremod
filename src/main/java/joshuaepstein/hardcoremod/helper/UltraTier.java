package joshuaepstein.hardcoremod.helper;

import joshuaepstein.hardcoremod.init.*;
import net.minecraft.item.*;
import net.minecraft.item.crafting.*;

public class UltraTier implements IItemTier {
    @Override
    public int getUses(){
        return ItemTier.NETHERITE.getUses() + 1000;
    }
    @Override
    public float getSpeed(){
        return ItemTier.NETHERITE.getSpeed() + 4;
    }
    @Override
    public float getAttackDamageBonus() {
        return 0F;
    }
    @Override
    public int getLevel() {
        return ItemTier.NETHERITE.getLevel();
    }
    @Override
    public int getEnchantmentValue() {
        return 15;
    }

    @Override
    public Ingredient getRepairIngredient() {
        return Ingredient.of(ModItems.ULTRA_INGOT);
    }
}
