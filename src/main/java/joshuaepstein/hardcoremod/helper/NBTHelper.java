package joshuaepstein.hardcoremod.helper;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.nbt.StringNBT;
import net.minecraftforge.common.util.Constants;

import java.util.*;
import java.util.function.Function;

public class NBTHelper {

    public static <T, N extends INBT> Map<UUID, T> readMap(CompoundNBT nbt, String name, Class<N> nbtType, Function<N, T> mapper) {
        Map<UUID, T> res = new HashMap<>();
        ListNBT uuidList = nbt.getList(name + "Keys", Constants.NBT.TAG_STRING);
        ListNBT valuesList = (ListNBT) nbt.get(name + "Values");

        if (uuidList.size() != valuesList.size()) {
            throw new IllegalStateException("Map doesn't have the same amount of keys as values");
        }

        for (int i = 0; i < uuidList.size(); i++) {
            res.put(UUID.fromString(uuidList.get(i).getAsString()), mapper.apply((N) valuesList.get(i)));
        }

        return res;
    }

    public static <T, N extends INBT> void writeMap(CompoundNBT nbt, String name, Map<UUID, T> map, Class<N> nbtType, Function<T, N> mapper) {
        ListNBT uuidList = new ListNBT();
        ListNBT valuesList = new ListNBT();
        map.forEach((key, value) -> {
            uuidList.add(StringNBT.valueOf(key.toString()));
            valuesList.add(mapper.apply(value));
        });
        nbt.put(name + "Keys", uuidList);
        nbt.put(name + "Values", valuesList);
    }

    public static <T, N extends INBT> List<T> readList(CompoundNBT nbt, String name, Class<N> nbtType, Function<N, T> mapper) {
        List<T> res = new LinkedList<>();
        ListNBT listNBT = (ListNBT) nbt.get(name);

        for (int i = 0; i < listNBT.size(); i++) {
            res.add(mapper.apply((N)listNBT.get(i)));
        }

        return res;
    }

    public static <T, N extends INBT> void writeList(CompoundNBT nbt, String name, Collection<T> list, Class<N> nbtType, Function<T, N> mapper) {
        ListNBT listNBT = new ListNBT();
        list.forEach(item -> listNBT.add(mapper.apply(item)));
        nbt.put(name, listNBT);
    }
    public static void setTag(ItemStack stack, String key, INBT value) {
        getTagCompound(stack).put(key, value);
    }

    public static void setByte(ItemStack stack, String key, byte value) {
        getTagCompound(stack).putByte(key, value);
    }

    public static void setShort(ItemStack stack, String key, short value) {
        getTagCompound(stack).putShort(key, value);
    }

    public static void setInt(ItemStack stack, String key, int value) {
        getTagCompound(stack).putInt(key, value);
    }

    public static void setLong(ItemStack stack, String key, long value) {
        getTagCompound(stack).putLong(key, value);
    }

    public static void setFloat(ItemStack stack, String key, float value) {
        getTagCompound(stack).putFloat(key, value);
    }

    public static void setDouble(ItemStack stack, String key, double value) {
        getTagCompound(stack).putDouble(key, value);
    }

    public static void setString(ItemStack stack, String key, String value) {
        getTagCompound(stack).putString(key, value);
    }

    public static void setByteArray(ItemStack stack, String key, byte[] value) {
        getTagCompound(stack).putByteArray(key, value);
    }

    public static void setIntArray(ItemStack stack, String key, int[] value) {
        getTagCompound(stack).putIntArray(key, value);
    }

    public static void setBoolean(ItemStack stack, String key, boolean value) {
        getTagCompound(stack).putBoolean(key, value);
    }

    public static INBT getTag(ItemStack stack, String key) {
        return stack.hasTag() ? getTagCompound(stack).get(key) : null;
    }

    public static byte getByte(ItemStack stack, String key) {
        return stack.hasTag() ? getTagCompound(stack).getByte(key) : 0;
    }

    public static short getShort(ItemStack stack, String key) {
        return stack.hasTag() ? getTagCompound(stack).getShort(key) : 0;
    }

    public static int getInt(ItemStack stack, String key) {
        return stack.hasTag() ? getTagCompound(stack).getInt(key) : 0;
    }

    public static long getLong(ItemStack stack, String key) {
        return stack.hasTag() ? getTagCompound(stack).getLong(key) : 0L;
    }

    public static float getFloat(ItemStack stack, String key) {
        return stack.hasTag() ? getTagCompound(stack).getFloat(key) : 0.0F;
    }

    public static double getDouble(ItemStack stack, String key) {
        return stack.hasTag() ? getTagCompound(stack).getDouble(key) : 0.0D;
    }

    public static String getString(ItemStack stack, String key) {
        return stack.hasTag() ? getTagCompound(stack).getString(key) : "";
    }

    public static byte[] getByteArray(ItemStack stack, String key) {
        return stack.hasTag() ? getTagCompound(stack).getByteArray(key) : new byte[0];
    }

    public static int[] getIntArray(ItemStack stack, String key) {
        return stack.hasTag() ? getTagCompound(stack).getIntArray(key) : new int[0];
    }

    public static boolean getBoolean(ItemStack stack, String key) {
        return stack.hasTag() && getTagCompound(stack).getBoolean(key);
    }

    public static boolean hasKey(ItemStack stack, String key) {
        return stack.hasTag() && getTagCompound(stack).contains(key);
    }

    public static void flipBoolean(ItemStack stack, String key) {
        setBoolean(stack, key, !getBoolean(stack, key));
    }

    public static void removeTag(ItemStack stack, String key) {
        if (hasKey(stack, key)) {
            getTagCompound(stack).remove(key);
        }
    }

    public static void validateCompound(ItemStack stack) {
        if (!stack.hasTag()) {
            CompoundNBT tag = new CompoundNBT();
            stack.setTag(tag);
        }
    }

    public static CompoundNBT getTagCompound(ItemStack stack) {
        validateCompound(stack);
        return stack.getTag();
    }
}
