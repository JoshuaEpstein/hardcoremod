package joshuaepstein.hardcoremod.helper;

import joshuaepstein.hardcoremod.Main;
import joshuaepstein.hardcoremod.init.ModItems;
import net.minecraft.item.IItemTier;
import net.minecraft.item.ItemTier;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.Ingredient;

public class PaxelUltraTier implements IItemTier {
    @Override
    public int getUses(){
        return (2031 + Main.getRandomNumber(100, 400));
    }
    @Override
    public float getSpeed(){
        return 12F;
    }
    @Override
    public float getAttackDamageBonus() {
        return 5.6F;
    }
    @Override
    public int getLevel() {
        return 5;
    }
    @Override
    public int getEnchantmentValue() {
        return 1;
    }

    @Override
    public Ingredient getRepairIngredient() {
        return Ingredient.of(Items.AIR);
    }

}
