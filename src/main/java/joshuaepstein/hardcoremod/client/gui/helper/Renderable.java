package joshuaepstein.hardcoremod.client.gui.helper;

import com.mojang.blaze3d.matrix.*;

public interface Renderable {

    void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks);

}
