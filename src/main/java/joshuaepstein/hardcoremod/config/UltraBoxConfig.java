package joshuaepstein.hardcoremod.config;

import com.google.gson.annotations.Expose;
import joshuaepstein.hardcoremod.Main;
import joshuaepstein.hardcoremod.init.ModItems;
import joshuaepstein.hardcoremod.util.WeightedList;
import joshuaepstein.hardcoremod.vending.Product;
import net.minecraft.item.Items;

public class UltraBoxConfig extends Config {

	@Expose public WeightedList<Product> POOL = new WeightedList<>();

	@Override
	public String getName() {
		return "ultra_box";
	}

	@Override
	protected void reset() {
		this.POOL.add(new Product(ModItems.MAGNETITE, Main.getRandomNumber(4, 8), null), 30);
		this.POOL.add(new Product(ModItems.ULTRA_NUGGET, 2, null), 23);
		this.POOL.add(new Product(ModItems.ULTRA_SCRAP, 2, null), 20);
		this.POOL.add(new Product(ModItems.SPAWNER_DUST, Main.getRandomNumber(1, 5), null), 22);
	}

}
