package joshuaepstein.hardcoremod.event;

import com.google.common.collect.Sets;
import joshuaepstein.hardcoremod.Main;
import joshuaepstein.hardcoremod.config.entry.EnchantedBookEntry;
import joshuaepstein.hardcoremod.init.ModConfigs;
import joshuaepstein.hardcoremod.init.ModItems;
import joshuaepstein.hardcoremod.item.MagnetItem;
import joshuaepstein.hardcoremod.util.OverlevelEnchantHelper;
import net.minecraft.block.material.Material;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.event.AnvilUpdateEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import org.lwjgl.system.CallbackI;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.FORGE)
public class AnvilEvents {

    @SubscribeEvent
    public static void onAnvilUpdate(AnvilUpdateEvent event) {
        ItemStack equipment = event.getLeft();
        ItemStack enchantedBook = event.getRight();

        if (equipment.getItem() == Items.ENCHANTED_BOOK) return;
        if (enchantedBook.getItem() != Items.ENCHANTED_BOOK) return;

        ItemStack upgradedEquipment = equipment.copy();

        Map<Enchantment, Integer> equipmentEnchantments = OverlevelEnchantHelper.getEnchantments(equipment);
        Map<Enchantment, Integer> bookEnchantments = OverlevelEnchantHelper.getEnchantments(enchantedBook);
        int overlevels = OverlevelEnchantHelper.getOverlevels(enchantedBook);

        if (overlevels == -1) return; // No over-levels, let vanilla do its thing

        Map<Enchantment, Integer> enchantmentsToApply = new HashMap<>(equipmentEnchantments);

        for (Enchantment bookEnchantment : bookEnchantments.keySet()) {
            if (!equipmentEnchantments.containsKey(bookEnchantment)) continue;
            int currentLevel = equipmentEnchantments.getOrDefault(bookEnchantment, 0);
            int bookLevel = bookEnchantments.get(bookEnchantment);
            int nextLevel = currentLevel == bookLevel ? currentLevel + 1 : Math.max(currentLevel, bookLevel);
            enchantmentsToApply.put(bookEnchantment, nextLevel);
        }

        EnchantmentHelper.setEnchantments(enchantmentsToApply, upgradedEquipment);

        if (upgradedEquipment.equals(equipment, true)) {
            event.setCanceled(true);
        } else {
            EnchantedBookEntry bookTier = ModConfigs.OVERLEVEL_ENCHANT.getTier(overlevels);
            event.setOutput(upgradedEquipment);
            event.setCost(bookTier == null ? 1 : bookTier.getLevelNeeded());
        }
    }
    // Adding the paxel charm will add the NBT data which will then be added to the output which is just a copy of the leftItem
    @SubscribeEvent
    public static void addPaxelCharm(AnvilUpdateEvent event){
        ItemStack rightItem = event.getRight();
        ItemStack leftItem = event.getLeft();
        ItemStack output = event.getLeft().copy();
        CompoundNBT nbt = null;
        Set<Item> ALL_PAXELS = Sets.newHashSet(ModItems.REACH_PAXEL, ModItems.RUSH_PAXEL, ModItems.FORTUNE_PAXEL);
        if(ALL_PAXELS.contains(leftItem.getItem())){
            if(rightItem.getItem().equals(ModItems.PAXEL_CHARM)){
                if(leftItem.hasTag()){
                    nbt = output.getOrCreateTag();
                }
                if(event.getLeft().getTag().getBoolean("hardcoremod:added_charm_paxel")){
                    return;
                }
                if(event.getLeft().getItem().equals(ModItems.FORTUNE_PAXEL)){
                    output.enchant(Enchantments.BLOCK_FORTUNE, 4);
                }
                nbt.putBoolean("hardcoremod:added_charm_paxel", true);
                output.setTag(nbt);
                event.setMaterialCost(1);
                event.setCost(21);
                event.setOutput(output);
            }
        }
        if(leftItem.getItem() == ModItems.BASIC_PAXEL){
            if(rightItem.getItem().equals(ModItems.PAXEL_CHARM)) {
                ItemStack UNIDENTIFIED_PAXEL = new ItemStack(ModItems.UNIDENTIFIED_PAXEL, 1);
                nbt = leftItem.getOrCreateTag();
                UNIDENTIFIED_PAXEL.setTag(nbt);
                event.setOutput(UNIDENTIFIED_PAXEL);
                event.setCost(Main.getRandomNumber(10, 27));
                event.setMaterialCost(1);
            }
        }
    }
}