package joshuaepstein.hardcoremod.event;


import joshuaepstein.hardcoremod.Main;
import joshuaepstein.hardcoremod.enchantments.Soulbound;
import net.minecraft.loot.LootPool;
import net.minecraft.loot.TableLootEntry;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.LootTableLoadEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = Main.MOD_ID, bus = Mod.EventBusSubscriber.Bus.FORGE)
public class LootTables {
    @SubscribeEvent
    public static void bastionBridgeInject(LootTableLoadEvent event){
        if(!event.isCancelable()) {
            if (event.getName().equals(new ResourceLocation("minecraft", "chests/bastion_bridge")) || event.getName().equals(new ResourceLocation("minecraft", "chests/bastion_other")) || event.getName().equals(new ResourceLocation("minecraft", "chests/bastion_treasure")) || event.getName().equals(new ResourceLocation("minecraft", "chests/end_city_treasure"))) {
                event.getTable().addPool(LootPool.lootPool().name("souldboundloot").add(TableLootEntry.lootTableReference(new ResourceLocation(Main.MOD_ID, "chests/soulbound_loot"))).build());
            }
        }
        }
}
