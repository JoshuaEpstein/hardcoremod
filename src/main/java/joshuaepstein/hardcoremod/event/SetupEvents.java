package joshuaepstein.hardcoremod.event;

import joshuaepstein.hardcoremod.Main;
import joshuaepstein.hardcoremod.init.*;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLDedicatedServerSetupEvent;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class SetupEvents {

    @SubscribeEvent
    public static void setupClient(final FMLClientSetupEvent event) {
        Main.LOGGER.info("setupClient()");
        ModScreens.register(event);
        ModScreens.registerOverlays();
        ModBlocks.registerTileEntityRenderers();
    }

    @SubscribeEvent
    public static void setupCommon(final FMLCommonSetupEvent event) {
        Main.LOGGER.info("setupCommon()");
        ModConfigs.register();
        ModNetwork.initialize();
    }

    @SubscribeEvent
    public static void setupDedicatedServer(final FMLDedicatedServerSetupEvent event) {
        Main.LOGGER.info("setupDedicatedServer()");
    }

}
