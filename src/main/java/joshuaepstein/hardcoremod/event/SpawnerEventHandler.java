package joshuaepstein.hardcoremod.event;

import java.util.Random;

import joshuaepstein.hardcoremod.Main;
import joshuaepstein.hardcoremod.init.ModItems;
import joshuaepstein.hardcoremod.lists.EnchantmentList;
import joshuaepstein.hardcoremod.network.packet.SpawnerModPacketHandler;
import joshuaepstein.hardcoremod.network.packet.SyncSpawnerEggDrop;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.SpawnEggItem;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.tileentity.MobSpawnerTileEntity;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EntityDamageSource;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.spawner.AbstractSpawner;
import net.minecraftforge.event.entity.living.LivingDropsEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.registries.ForgeRegistries;
import org.lwjgl.system.CallbackI;

/**
 * 	Handles all the events regarding the mob spawner and entities.
 *
 * 	@author Anders <Branders> Blomqvist
 */
@EventBusSubscriber
public class SpawnerEventHandler {

    private Random random = new Random();

    @SubscribeEvent
    public void onBlockBreakEvent(BlockEvent.BreakEvent event) {
        if(event.getState().getBlock() == Blocks.SPAWNER) {
            World world = event.getPlayer().level;
            for (int i = 0; i < 10; i++) {
                double x = (double) event.getPos().getX() + random.nextDouble();
                double y = (double) event.getPos().getY() + random.nextDouble();
                double z = (double) event.getPos().getZ() + random.nextDouble();
                event.getWorld().addParticle(ParticleTypes.SMOKE, x, y, z, 0.0D, 0.0D, 0.0D);
                event.getWorld().addParticle(ParticleTypes.FLAME, x, y, z, 0.0D, 0.0D, 0.0D);
            }
            BlockState blockstate = world.getBlockState(event.getPos());
            MobSpawnerTileEntity spawner = (MobSpawnerTileEntity)world.getBlockEntity(event.getPos());
            AbstractSpawner logic = spawner.getSpawner();
            CompoundNBT nbt = new CompoundNBT();
            nbt = logic.save(nbt);
            String entity_string = nbt.get("SpawnData").toString();
            entity_string = entity_string.substring(entity_string.indexOf("\"") + 1);
            entity_string = entity_string.substring(0, entity_string.indexOf("\""));
            if(entity_string.equalsIgnoreCase(EntityType.AREA_EFFECT_CLOUD.getRegistryName().toString()))
                return;
            ItemStack itemStack;
            if(entity_string.contains("iron_golem"))
                return;
            if(entity_string.contains("villager"))
                return;
            else
                itemStack = new ItemStack(
                        ForgeRegistries.ITEMS.getValue(new ResourceLocation(entity_string + "_spawn_egg")));
            ItemEntity spawnerDust = new ItemEntity(world, event.getPos().getX(), event.getPos().getY(), event.getPos().getZ(), new ItemStack(ModItems.SPAWNER_DUST, Main.getRandomNumber(1, 2)));
            if(!world.isClientSide()) {
                if(EnchantmentHelper.getEnchantments(event.getPlayer().getItemInHand(event.getPlayer().swingingArm)).containsKey(Enchantments.BLOCK_FORTUNE)){
                    spawnerDust = new ItemEntity(world, event.getPos().getX(), event.getPos().getY(), event.getPos().getZ(), new ItemStack(ModItems.SPAWNER_DUST, Main.getRandomNumber(EnchantmentHelper.getEnchantmentLevel(Enchantments.BLOCK_FORTUNE, event.getPlayer())*2, EnchantmentHelper.getEnchantmentLevel(Enchantments.BLOCK_FORTUNE, event.getPlayer())*3)));
                }
                world.addFreshEntity(spawnerDust);
//                event.getState().getBlock()
                if(EnchantmentHelper.getEnchantments(event.getPlayer().getItemInHand(event.getPlayer().swingingArm)).containsKey(EnchantmentList.SPAWNER_EGG.get())){
                    world.addFreshEntity(new ItemEntity(world, event.getPos().getX(), event.getPos().getY(), event.getPos().getZ(), itemStack));
                }
            } else {
                event.getPlayer().drop(new ItemStack(ModItems.SPAWNER_DUST, Main.getRandomNumber(0, 3)), true);

            }
        }
    }

    /**
     * 	Enables mobs to have a small chance to drop an egg
     */
    @SubscribeEvent
    public void onMobDrop(LivingDropsEvent event) {

        Entity entity = event.getEntity();
        EntityType<?> entityType = entity.getType();

        ItemStack itemStack;

        // Leave if a player died.
        if(entityType.equals(EntityType.PLAYER))
            return;
        else if (entityType.equals(EntityType.VILLAGER) || entityType.equals(EntityType.ZOMBIE_VILLAGER))
            return;
        if(entityType.equals(EntityType.WITHER_SKELETON) || entityType.equals(EntityType.ZOMBIFIED_PIGLIN) || entityType.equals(EntityType.STRAY))
            return;
        else
            itemStack = new ItemStack(ForgeRegistries.ITEMS
                    .getValue(new ResourceLocation(entityType.getRegistryName() + "_spawn_egg")));

        // Add monster egg to drops
        PlayerEntity player = null;
        Entity d = event.getSource().getDirectEntity();
        if (d instanceof PlayerEntity) {
            player = (PlayerEntity) d;
        }
        Random rand = new Random();
        boolean val = new Random().nextInt(15)==0;
        if (d instanceof PlayerEntity) {
            if (EnchantmentHelper.getEnchantments(player.getItemInHand(player.swingingArm)).containsKey(Enchantments.MOB_LOOTING)) {
                val = new Random().nextInt(15/EnchantmentHelper.getEnchantmentLevel(Enchantments.MOB_LOOTING, player)) ==0;
            }
            if (val) {
                event.getDrops().add(new ItemEntity(entity.level, entity.getX(), entity.getY(), entity.getZ(), itemStack));
            } else {
                return;
            }
        }
    }
}
