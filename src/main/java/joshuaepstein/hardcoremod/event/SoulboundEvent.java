package joshuaepstein.hardcoremod.event;

import joshuaepstein.hardcoremod.Main;
import joshuaepstein.hardcoremod.util.*;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.player.*;
import net.minecraftforge.event.entity.living.LivingDropsEvent;
import net.minecraftforge.event.entity.player.*;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = Main.MOD_ID, bus = Mod.EventBusSubscriber.Bus.FORGE)
public class SoulboundEvent {

    @SubscribeEvent(priority = EventPriority.HIGH)
    public static void retrievalEvent(LivingDropsEvent event)
    {
        if (event.getEntity() instanceof PlayerEntity) {
            SoulboundHandler.getOrCreateSoulboundHandler((PlayerEntity)event.getEntityLiving()).retainDrops(event.getDrops());
        }
    }

    @SubscribeEvent
    public static void itemTransferEvent(PlayerEvent.Clone event)
    {
        if (event.isWasDeath()) {
            PlayerEntity oldPlayer = event.getOriginal();
            if (SoulboundHandler.hasStoredDrops(oldPlayer)) {
                SoulboundHandler.getOrCreateSoulboundHandler(oldPlayer).transferItems(event.getPlayer());
            } else if (SoulboundHandler.hasStoredDrops(event.getPlayer())) {
                SoulboundHandler.getOrCreateSoulboundHandler(event.getPlayer()).transferItems(event.getPlayer());
            }
        }
    }

}