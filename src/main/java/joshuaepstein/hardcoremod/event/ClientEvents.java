package joshuaepstein.hardcoremod.event;

import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.ColorHandlerEvent;
import net.minecraftforge.eventbus.api.*;
import net.minecraftforge.fml.common.Mod;
import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.util.math.MathHelper;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD, value = {Dist.CLIENT})
public class ClientEvents {

	@SubscribeEvent
	public static void onColorHandlerRegister(ColorHandlerEvent.Item event) {
//		ModModels.registerItemColors(event.getItemColors());
	}



}
