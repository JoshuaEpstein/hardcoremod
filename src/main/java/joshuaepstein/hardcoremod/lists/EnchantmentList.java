package joshuaepstein.hardcoremod.lists;

import joshuaepstein.hardcoremod.Main;
import joshuaepstein.hardcoremod.enchantments.Soulbound;
import joshuaepstein.hardcoremod.enchantments.SpawnerSpawnEgg;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentType;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class EnchantmentList {

    public static final DeferredRegister<Enchantment> ENCHANTMENTS = DeferredRegister.create(ForgeRegistries.ENCHANTMENTS, Main.MOD_ID);

    public static final RegistryObject<Enchantment> SOULBOUND = ENCHANTMENTS.register("soulbound", () -> new Soulbound(Enchantment.Rarity.RARE, EnchantmentType.VANISHABLE, EquipmentSlotType.values()));
    public static final RegistryObject<Enchantment> SPAWNER_EGG = ENCHANTMENTS.register("spawner_egg_drop", () -> new SpawnerSpawnEgg(Enchantment.Rarity.RARE, EnchantmentType.VANISHABLE, EquipmentSlotType.values()));



}