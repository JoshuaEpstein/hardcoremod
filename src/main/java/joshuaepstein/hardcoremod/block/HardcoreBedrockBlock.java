package joshuaepstein.hardcoremod.block;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class HardcoreBedrockBlock extends Block {

	public HardcoreBedrockBlock() {
		super(AbstractBlock.Properties.of(Material.STONE)
				.strength(-1.0F, 3600000.0F)
				.noDrops().isValidSpawn((a, b, c, d) -> false));
	}

}
