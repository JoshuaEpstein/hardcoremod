package joshuaepstein.hardcoremod.block;

import joshuaepstein.hardcoremod.Main;
import net.minecraft.block.OreBlock;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;

import java.util.Random;

import net.minecraft.block.AbstractBlock.Properties;
import net.minecraftforge.common.ToolType;

public class HardcoreOreBlock extends OreBlock {

    public HardcoreOreBlock() {
        super(Properties.of(Material.STONE, MaterialColor.DIAMOND)
                .requiresCorrectToolForDrops()
                .harvestTool(ToolType.PICKAXE)
                .harvestLevel(3)
                .lightLevel(state -> 9)
                .strength(3f, 3f)
        );
    }

    @Override
    protected int xpOnDrop(Random rand) {
        return Main.getRandomNumber(10, 17);
    }

}
