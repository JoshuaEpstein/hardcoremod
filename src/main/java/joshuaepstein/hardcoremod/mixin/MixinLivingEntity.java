package joshuaepstein.hardcoremod.mixin;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.attributes.Attribute;
import net.minecraft.entity.ai.attributes.AttributeModifierMap;
import net.minecraft.entity.ai.attributes.ModifiableAttributeInstance;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.potion.Effect;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import javax.annotation.Nullable;

@Mixin(LivingEntity.class)
public abstract class MixinLivingEntity extends Entity {

	public MixinLivingEntity(EntityType<?> entityType, World world) {
		super(entityType, world);
	}

	@Shadow public abstract EffectInstance getEffect(Effect potionIn);

	@Shadow @Nullable public abstract ModifiableAttributeInstance getAttribute(Attribute attribute);

	@Shadow public abstract boolean hasEffect(Effect potionIn);

	@Redirect(method = "createLivingAttributes", at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/ai/attributes/AttributeModifierMap;builder()Lnet/minecraft/entity/ai/attributes/AttributeModifierMap$MutableAttribute;"))
	private static AttributeModifierMap.MutableAttribute registerAttributes() {
		return AttributeModifierMap.builder();
	}

}
