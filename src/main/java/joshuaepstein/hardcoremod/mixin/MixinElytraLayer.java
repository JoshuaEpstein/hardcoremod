package joshuaepstein.hardcoremod.mixin;

import net.minecraft.client.renderer.entity.IEntityRenderer;
import net.minecraft.client.renderer.entity.layers.ElytraLayer;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(ElytraLayer.class)
public abstract class MixinElytraLayer<T extends LivingEntity, M extends EntityModel<T>> extends LayerRenderer<T, M>  {

	public MixinElytraLayer(IEntityRenderer<T, M> renderer) {
		super(renderer);
	}

	@Inject(method = "shouldRender", at = @At("HEAD"), cancellable = true, remap = false)
	public void shouldRender(ItemStack stack, T entity, CallbackInfoReturnable<Boolean> ci) {
// When there is something needed here, It will be added
		//TODO: When Armor added, this will be needed
	}

}
