package joshuaepstein.hardcoremod.mixin;

import joshuaepstein.hardcoremod.item.*;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.ItemStack;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(Enchantment.class)
public abstract class MixinEnchantment {

	@Inject(method = "canEnchant", at = @At("HEAD"), cancellable = true)
	private void canEnchant(ItemStack stack, CallbackInfoReturnable<Boolean> ci) {
//		if(stack.getItem() instanceof AxeItem || stack.getItem() instanceof PickaxeItem || stack.getItem() instanceof ShovelItem || stack.getItem() instanceof PaxelItem || stack.getItem() instanceof HoeItem || stack.getItem() instanceof SwordItem) {
//			ci.setReturnValue(false);
//		}
	}

}
