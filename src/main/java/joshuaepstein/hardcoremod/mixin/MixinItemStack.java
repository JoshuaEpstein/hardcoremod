package joshuaepstein.hardcoremod.mixin;

import joshuaepstein.hardcoremod.item.*;
import net.minecraft.advancements.CriteriaTriggers;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.enchantment.UnbreakingEnchantment;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.IFormattableTextComponent;
import net.minecraft.util.text.ITextComponent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import javax.annotation.Nullable;
import java.util.Random;

@Mixin(value = ItemStack.class, priority = 1001)
public abstract class MixinItemStack {

	@Shadow public abstract boolean isDamageableItem();
	@Shadow public abstract int getDamageValue();
	@Shadow public abstract void setDamageValue(int damage);
	@Shadow public abstract int getMaxDamage();

	@Shadow public abstract ItemStack copy();

	@Shadow public abstract Item getItem();

	/**
	 * @author Vault (Iskallia)
	 * @reason Made for the Vault Mod by Iskall85 - Vault Hunters 2021©
	 */
	@Overwrite

	public boolean hurt(int amount, Random rand, @Nullable ServerPlayerEntity damager) {
		if(!this.isDamageableItem()) return false;

		if(amount > 0) {
			int i = EnchantmentHelper.getItemEnchantmentLevel(Enchantments.UNBREAKING, (ItemStack)(Object)this);
			int j = 0;

			for(int k = 0; i > 0 && k < amount; ++k) {
				if(UnbreakingEnchantment.shouldIgnoreDurabilityDrop((ItemStack)(Object)this, i, rand)) {
					++j;
				}
			}

			amount -= j;

			if(amount <= 0) {
				return false;
			}
		}

		if(damager != null && amount != 0) {
			CriteriaTriggers.ITEM_DURABILITY_CHANGED.trigger(damager, (ItemStack)(Object)this, this.getDamageValue() + amount);
		}

		int l = this.getDamageValue() + amount;
		this.setDamageValue(l);

		return l >= this.getMaxDamage();
	}

	@Inject(method = "getDisplayName", at = @At("RETURN"), cancellable = true)
	public void useGearRarity(CallbackInfoReturnable<ITextComponent> ci) {
		if (!(getItem() instanceof AxeItem || getItem() instanceof PickaxeItem || getItem() instanceof ShovelItem || getItem() instanceof HoeItem || getItem() instanceof SwordItem)) {
			return;
		}

		ItemStack itemStack = this.copy();


		IFormattableTextComponent returnValue = (IFormattableTextComponent) ci.getReturnValue();
	}

}
