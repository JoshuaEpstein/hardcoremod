package joshuaepstein.hardcoremod.network.packet;

import joshuaepstein.hardcoremod.Main;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkRegistry;
import net.minecraftforge.fml.network.simple.SimpleChannel;

/**
 * 	Class to handle network message for synchronizing spawner nbt values
 *
 * 	@author Anders <Branders> Blomqvist
 *
 */
public class SpawnerModPacketHandler
{
    private static final String PROTOCOL_VERSION = "1";

    public static final SimpleChannel INSTANCE = NetworkRegistry.newSimpleChannel(
            new ResourceLocation(Main.MOD_ID, "main"),
            () -> PROTOCOL_VERSION,
            PROTOCOL_VERSION::equals,
            PROTOCOL_VERSION::equals
    );

    public static void register()
    {
        int messageId = 0;


        INSTANCE.registerMessage(messageId++,
                SyncSpawnerEggDrop.class,
                SyncSpawnerEggDrop::encode,
                SyncSpawnerEggDrop::decode,
                SyncSpawnerEggDrop::handle);

    }
}