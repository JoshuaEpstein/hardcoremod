package joshuaepstein.hardcoremod.network.packet;

import joshuaepstein.hardcoremod.Main;
import net.minecraft.block.BlockState;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.MobSpawnerTileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.spawner.AbstractSpawner;
import net.minecraftforge.fml.network.NetworkEvent;
import net.minecraftforge.registries.ForgeRegistries;
import java.util.function.Supplier;
public class SyncSpawnerEggDrop
{
    private final BlockPos pos;

    public SyncSpawnerEggDrop(BlockPos pos)
    {
        this.pos = pos;
    }

    public static void encode(SyncSpawnerEggDrop msg, PacketBuffer buf)
    {
        buf.writeBlockPos(msg.pos);
    }

    public static SyncSpawnerEggDrop decode(PacketBuffer buf)
    {
        BlockPos pos = new BlockPos(buf.readBlockPos());

        return new SyncSpawnerEggDrop(pos);
    }

    public static void handle(SyncSpawnerEggDrop msg, Supplier<NetworkEvent.Context> ctx)
    {
        ctx.get().enqueueWork(() -> {

            World world = ctx.get().getSender().level;

            if(world != null) {

                MobSpawnerTileEntity spawner = (MobSpawnerTileEntity)world.getBlockEntity(msg.pos);
                AbstractSpawner logic = spawner.getSpawner();

                // Get entity ResourceLocation string from spawner by creating a empty compound which we make our
                // spawner logic write to. We can then access what type of entity id the spawner has inside
                CompoundNBT nbt = new CompoundNBT();
                nbt = logic.save(nbt);
                String entity_string = nbt.get("SpawnData").toString();

                entity_string = entity_string.substring(entity_string.indexOf("\"") + 1);
                entity_string = entity_string.substring(0, entity_string.indexOf("\""));

                // Get the entity mob egg and put in an ItemStack
                ItemStack itemStack;
                if(entity_string.contains("villager"))
                    return;
                else
                    itemStack = new ItemStack(ForgeRegistries.ITEMS.getValue(new ResourceLocation(entity_string + "_spawn_egg")));

                // Get random fly-out position offsets
                double d0 = (double)(world.getRandom().nextFloat() * 0.7F) + (double)0.15F;
                double d1 = (double)(world.getRandom().nextFloat() * 0.7F) + (double)0.06F + 0.6D;
                double d2 = (double)(world.getRandom().nextFloat() * 0.7F) + (double)0.15F;

                // Create entity item
                ItemEntity entityItem = new ItemEntity(world, (double)msg.pos.getX() + d0, (double)msg.pos.getY() + d1, (double)msg.pos.getZ() + d2, itemStack);
                entityItem.setNoPickUpDelay();


                // Spawn entity item (egg)
                if(Main.randomChance()){
                    world.addFreshEntity(entityItem);
                }
            }
        });

        ctx.get().setPacketHandled(true);
    }
}