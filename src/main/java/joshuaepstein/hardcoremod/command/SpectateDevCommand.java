package joshuaepstein.hardcoremod.command;

import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import joshuaepstein.hardcoremod.Main;
import joshuaepstein.hardcoremod.enchantments.Soulbound;
import joshuaepstein.hardcoremod.lists.EnchantmentList;
import joshuaepstein.hardcoremod.util.SoulboundHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.command.CommandSource;
import net.minecraft.dispenser.Position;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.GameType;

import java.util.Random;

public class SpectateDevCommand extends Command {
    double X;
    double Y;
    double Z;
    @Override
    public String getName() {
        return "spectate-dev";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 2;
    }
    @Override
    public void build(LiteralArgumentBuilder<CommandSource> builder) {
        builder.executes(this::spectateDev);
    }

    private int spectateDev(CommandContext<CommandSource> context) throws CommandSyntaxException {
        ServerPlayerEntity player = context.getSource().getPlayerOrException();

        if(player.getDisplayName().equals("Dev")){
            X = context.getSource().getPosition().x;
            Y = context.getSource().getPosition().y;
            Z = context.getSource().getPosition().z;
            Vector3d old = context.getSource().getPosition();
            Position newX = new Position(2, 3, 4);
            player.setPos(X, Y, Z);
            player.setGameMode(GameType.SPECTATOR);
        }
        return 0;
    }

    @Override
    public boolean isDedicatedServerOnly() {
        return false;
    }

}
