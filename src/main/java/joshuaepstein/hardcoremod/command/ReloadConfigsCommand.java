package joshuaepstein.hardcoremod.command;

import com.mojang.brigadier.builder.*;
import com.mojang.brigadier.context.*;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import joshuaepstein.hardcoremod.Main;
import joshuaepstein.hardcoremod.init.*;
import net.minecraft.command.*;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextComponent;

public class ReloadConfigsCommand extends Command {

    @Override
    public String getName() {
        return "reloadcfg";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 2;
    }

    @Override
    public void build(LiteralArgumentBuilder<CommandSource> builder) {
        builder.executes(this::reloadConfigs);
    }

    private int reloadConfigs(CommandContext<CommandSource> context) throws CommandSyntaxException {
        ServerPlayerEntity player = context.getSource().getPlayerOrException();
        String mcNick = player.getDisplayName().getString();
        player.displayClientMessage(new StringTextComponent("You have reloaded the configs"), true);
        try { ModConfigs.register(); } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return 0;
    }

    @Override
    public boolean isDedicatedServerOnly() {
        return false;
    }

}