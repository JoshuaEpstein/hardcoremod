package joshuaepstein.hardcoremod.command;

import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import joshuaepstein.hardcoremod.Main;
import net.minecraft.command.CommandSource;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;

public class VersionCommand extends Command {

    @Override
    public String getName() {
        return "version";
    }

    @Override
    public int getRequiredPermissionLevel() {
        return 0;
    }

    @Override
    public void build(LiteralArgumentBuilder<CommandSource> builder) {
        builder.executes(this::version);
    }

    private int version(CommandContext<CommandSource> context) throws CommandSyntaxException {
        ServerPlayerEntity player = context.getSource().getPlayerOrException();
        String mcNick = player.getDisplayName().getString();
        player.sendMessage(new StringTextComponent(TextFormatting.GRAY + "You are currently running version " + TextFormatting.BLUE + Main.MOD_VERSION + TextFormatting.GRAY + " of the HardcoreMod by JoshuaEpstein\n\n"), player.getUUID());
        player.sendMessage(new StringTextComponent(TextFormatting.GRAY + "To view version " + TextFormatting.BLUE + Main.MOD_VERSION + TextFormatting.GRAY + "'s patch notes. Go to dev.joshuaepstein.co.uk/hardcoremod/patch-1.2"), player.getUUID());
        return 0;
    }

    @Override
    public boolean isDedicatedServerOnly() {
        return false;
    }

}
