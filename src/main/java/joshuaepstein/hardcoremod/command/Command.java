package joshuaepstein.hardcoremod.command;

import com.mojang.brigadier.builder.*;
import com.mojang.brigadier.context.*;
import net.minecraft.command.*;
import net.minecraft.util.text.*;

public abstract class Command {

    public abstract String getName();

    public abstract int getRequiredPermissionLevel();

    public abstract void build(LiteralArgumentBuilder<CommandSource> builder);

    public abstract boolean isDedicatedServerOnly();

    protected final void sendFeedback(CommandContext<CommandSource> context, String message, boolean showOps) {
        context.getSource().sendSuccess(new StringTextComponent(message), showOps);
    }

}
