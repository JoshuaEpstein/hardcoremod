[![GNU License][license-shield]][license-url]


[license-shield]: https://img.shields.io/github/v/release/joshuaepstein/hardcoremod?include_prereleases&style=for-the-badge
[license-url]: https://github.com/joshuaepstein/hardcoremod/releases/tag/v1.2
